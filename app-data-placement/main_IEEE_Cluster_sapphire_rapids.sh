#!/bin/zsh

# ==============================================================
# === User and machine specific settings
# ==============================================================
# get directory of current script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export H2M_REPO_ROOT=/home/sdp/cfoyer
export H2M_APP_ROOT=/home/sdp/cfoyer/applications
export H2M_INCLUDE_DIR=/home/sdp/cfoyer/h2m/INSTALL/include
export RECOMMENDER_SCRIPT=${H2M_REPO_ROOT}/h2m-recommender/scripts/generate_trait_recommendations.py
export RECOMMENDER_MEM_CFG=${H2M_REPO_ROOT}/h2m-recommender/configs/sapphire-rapids-hbm-dram.json
export CFG_NAME=rwth-optane-gcc
export N_THREADS=10
export N_REPS=3

# specify the overarching result root
export MAIN_RESULT_DIR=${SCRIPT_DIR}/results_sapphire_rapids

# ensure that all generic scripts are executable
find ${SCRIPT_DIR} -name '*.sh' -exec chmod u+x '{}' ';'

# ==============================================================
# === Execute desired benchmarks
# ==============================================================

echo "=============================================================="
echo "=== multi-phase-app"
echo "=============================================================="
export APP_DIR=${H2M_REPO_ROOT}/h2m/examples/codes/multiple_phases
export RESULT_ROOT=${MAIN_RESULT_DIR}/multi-phase-app
rm -rf ${RESULT_ROOT}
cd ${SCRIPT_DIR}/multi-phase-app
./main.sh


echo "=============================================================="
echo "=== XSBench"
echo "=============================================================="
export APP_DIR=${H2M_APP_ROOT}/xsbench/openmp-threading
export RESULT_ROOT=${MAIN_RESULT_DIR}/XSBench
rm -rf ${RESULT_ROOT}
cd ${SCRIPT_DIR}/XSBench
./main.sh


echo "=============================================================="
echo "=== LULESH"
echo "=============================================================="
export APP_DIR=${H2M_APP_ROOT}/LULESH
export RESULT_ROOT=${MAIN_RESULT_DIR}/LULESH
rm -rf ${RESULT_ROOT}
cd ${SCRIPT_DIR}/LULESH
./main.sh


echo "=============================================================="
echo "=== NPB-BT"
echo "=============================================================="
export NPB_ROOT=${H2M_APP_ROOT}/npb/NPB3.4-OMP
export RESULT_ROOT=${MAIN_RESULT_DIR}/NPB
rm -rf ${RESULT_ROOT}_*
cd ${SCRIPT_DIR}/NPB
./main_BT.sh
# echo "=============================================================="
# echo "=== NPB-SP"
# echo "=============================================================="
# ./main_SP.sh
# echo "=============================================================="
# echo "=== NPB-LU"
# echo "=============================================================="
# ./main_LU.sh


echo "=============================================================="
echo "=== miniFE"
echo "=============================================================="
export APP_DIR=${H2M_APP_ROOT}/miniFE/openmp/src
export RESULT_ROOT=${MAIN_RESULT_DIR}/miniFE
rm -rf ${RESULT_ROOT}
cd ${SCRIPT_DIR}/miniFE
./main.sh


echo "=============================================================="
echo "=== SPEC - 351.bwaves"
echo "=============================================================="
cd ${H2M_APP_ROOT}/SPEC-OMP2012
source shrc
export RESULT_ROOT=${MAIN_RESULT_DIR}/SPEC-OMP2012
rm -rf ${RESULT_ROOT}_*
cd ${SCRIPT_DIR}/SPEC-OMP2012
./SPEC_351.bwaves.sh
