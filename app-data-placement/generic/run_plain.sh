#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
EXEC_CMD=${EXEC_CMD:-$1}

if [[ -z "${EXEC_CMD}" ]]; then
    echo "ERROR: Parameter EXEC_CMD is not set."
    exit 2
fi

RESULT_DIR_PLAIN=${RESULT_DIR_PLAIN:-"$(pwd)/results_plain"}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}

# create result directory
mkdir -p ${RESULT_DIR_PLAIN}
cd ${RESULT_DIR_PLAIN}

# ========================================
# === Execution / profiling
# ========================================
echo "Running plain experiment"
OUT_NAME=${RESULT_DIR_PLAIN}/output_plain.log
numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} ${TIME_BIN:-/usr/bin/time} -v ${EXEC_CMD} &> ${OUT_NAME}

# exit without error if this point has been reached
exit 0
