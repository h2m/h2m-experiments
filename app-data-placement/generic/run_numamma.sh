#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
EXEC_CMD=${EXEC_CMD:-$1}

if [[ -z "${EXEC_CMD}" ]]; then
    echo "ERROR: Parameter EXEC_CMD is not set."
    exit 2
fi

RESULT_PREFIX_NUMAMMA=${RESULT_PREFIX_NUMAMMA:-"$(pwd)/results_numamma"}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}

# Note:
#   Sampling rate of 1000 is more than enough
#   Test revealed that it highly depends whether per-thread buffers run full.
#   For now alarm (collecting samples) of 100 is good.
# VALUES_ALARM=${VALUES_ALARM:-"100"}
VALUES_SAMPLING_RATE=${VALUES_SAMPLING_RATE:-"6000"}

NUMAMMA_BUFFER_SIZE=${NUMAMMA_BUFFER_SIZE:-"2048"}
case "`sed 'y/[A-Z]/[a-z]/' <<< ${NUMAMMA_FLUSH_FULL_SAMPLING}`" in
    0|no|disabled|false)
        NUMAMMA_FLUSH_FULL_SAMPLING="--flush=no"
        ;;
esac

# apply split to retrieve arrays
# IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_VALUES_ALARM <<< "${VALUES_ALARM}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_VALUES_SAMPLING_RATE <<< "${VALUES_SAMPLING_RATE}"

# ========================================
# === Execution / profiling
# ========================================

# for val_al in "${FINAL_VALUES_ALARM[@]}"
# do
for val_sr in "${FINAL_VALUES_SAMPLING_RATE[@]}"
do
    echo "Running experiment with sampling_rate=${val_sr}"

    # create directory
    RES_DIR=${RESULT_PREFIX_NUMAMMA}_sr_${val_sr}
    mkdir -p ${RES_DIR}
    cd ${RES_DIR}
    export H2M_DUMP_DIR=${RES_DIR}
    OUT_NAME=${RES_DIR}/output_numamma.log

    # execute
    eval "numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} \
        numamma --sampling-rate=${val_sr} --buffer-size=${NUMAMMA_BUFFER_SIZE} ${NUMAMMA_FLUSH_FULL_SAMPLING} -d -D --outputdir=${RES_DIR} -n -- \
            ${EXEC_CMD}" &> ${OUT_NAME} \
                || exit $?
done
# done

# exit without error if this point has been reached
exit 0
