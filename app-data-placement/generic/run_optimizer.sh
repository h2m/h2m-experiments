#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
RESULT_DIR_NUMAMMA=${RESULT_DIR_NUMAMMA:-$1}
RECOMMENDER_SCRIPT=${RECOMMENDER_SCRIPT:-$2}
RECOMMENDER_MEM_CFG=${RECOMMENDER_MEM_CFG:-$3}

if [[ -z "${RESULT_DIR_NUMAMMA}" ]]; then
    echo "ERROR: Parameter RESULT_DIR_NUMAMMA is not set."
    exit 2
fi
if [[ -z "${RECOMMENDER_MEM_CFG}" ]]; then
    echo "ERROR: Parameter RECOMMENDER_MEM_CFG is not set."
    exit 2
fi
if [[ -z "${RECOMMENDER_SCRIPT}" ]]; then
    echo "ERROR: Parameter RECOMMENDER_SCRIPT is not set."
    exit 2
fi

RECOMMENDER_AGGREGATE_BY=${RECOMMENDER_AGGREGATE_BY:-2}
CLASS_RECOMMENDER=${CLASS_RECOMMENDER:-"CTraitRecommender_Knapsack"}
CAP_LIMITS_MB=${CAP_LIMITS_MB:-"500,750,1000"}
N_THREADS=${N_THREADS:-"16"}
OBJ_FNC_VARIANTS=${OBJ_FNC_VARIANTS:-"GAIN_AND_TRANSFER_TIME,GAIN"}
PM_WEIGHTING_FACTORS=${PM_WEIGHTING_FACTORS:-"1"}
export KS_SAMPLING_FREQ=${KS_SAMPLING_FREQ:-6000}
export KS_ARR_NEXT_N_PHASES=${KS_ARR_NEXT_N_PHASES:-"1,2,3,all"}

if [[ "${KS_ARR_NEXT_N_PHASES}" == "none" ]] ; then
    unset KS_ARR_NEXT_N_PHASES
fi

# apply split to retrieve arrays
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_CAP_LIMITS_MB <<< "${CAP_LIMITS_MB}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_OBJ_FNC_VARIANTS <<< "${OBJ_FNC_VARIANTS}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_N_THREADS <<< "${N_THREADS}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_PM_WEIGHTING_FACTORS <<< "${PM_WEIGHTING_FACTORS}"

# ==================================================
# Gurobi specific
# ==================================================
export KS_SOLVER_TYPE="gurobi"

# ==================================================
# LPSolve specific
# ==================================================
# export KS_LPSOLVE_PATH=/work/jk869269/repos/hpc-projects/h2m/00_util/lp_solve_5.5.2.11_exe_ux64/lp_solve
# export KS_SOLVER_TYPE="lpsolve"

# ==================================================
# Generate statistics once
# ==================================================
for nth in "${FINAL_N_THREADS[@]}"
do
    for pmwf in "${FINAL_PM_WEIGHTING_FACTORS[@]}"
    do
        export OPT_PM_WEIGHTING_FACTOR=${pmwf}

        echo "Generating statistics for ${nth} threads and pmwf ${pmwf} ..."
        python3 ${RECOMMENDER_SCRIPT} \
            --source ${RESULT_DIR_NUMAMMA} \
            --dest_file_postfix "_${nth}-threads_pmwf-${pmwf}" \
            --aggregate_by 3 \
            --gen_stats 1 \
            --gen_traits 0 \
            --memory_cfg "${RECOMMENDER_MEM_CFG}" \
            --nr_threads ${nth}
    done
done

# ==================================================
# Generate traits / placement decisions
# ==================================================
for nth in "${FINAL_N_THREADS[@]}"
do
    for lim in "${FINAL_CAP_LIMITS_MB[@]}"
    do
        for variant in "${FINAL_OBJ_FNC_VARIANTS[@]}"
        do
            export KS_OBJ_FNC_VARIANT=${variant}

            if [[ "${variant}" == "GAIN_AND_TRANSFER_TIME" || "${variant}" == "GAIN_AND_TRANSFER_TIME_DECAY" ]] ; then
                for pmwf in "${FINAL_PM_WEIGHTING_FACTORS[@]}"
                do
                    export OPT_PM_WEIGHTING_FACTOR=${pmwf}
                    echo "Generating recommendations for ${nth} threads, pmwf ${pmwf} and HBW capacity limit ${lim} MB - Variant: ${variant} ..."
                    # Note: use --aggregate_by 1 for allocation abstraction
                    # Note: use --aggregate_by 2 for allocation interception
                    python3 ${RECOMMENDER_SCRIPT} \
                        --source ${RESULT_DIR_NUMAMMA} \
                        --dest_file_postfix "_${nth}-threads_pmwf-${pmwf}_${lim}MB_variant-${variant}" \
                        --aggregate_by ${RECOMMENDER_AGGREGATE_BY} \
                        --gen_stats 0 \
                        --gen_traits 1 \
                        --memory_cfg "${RECOMMENDER_MEM_CFG}" \
                        --class_recommender "${CLASS_RECOMMENDER}" \
                        --nr_threads ${nth} \
                        --cap_limit_MB_idx_0 ${lim}
                done
            else
                export OPT_PM_WEIGHTING_FACTOR=1
                echo "Generating recommendations for ${nth} threads, pmwf 1 and HBW capacity limit ${lim} MB - Variant: ${variant} ..."
                # Note: use --aggregate_by 1 for allocation abstraction
                # Note: use --aggregate_by 2 for allocation interception
                python3 ${RECOMMENDER_SCRIPT} \
                    --source ${RESULT_DIR_NUMAMMA} \
                    --dest_file_postfix "_${nth}-threads_pmwf-1_${lim}MB_variant-${variant}" \
                    --aggregate_by ${RECOMMENDER_AGGREGATE_BY} \
                    --gen_stats 0 \
                    --gen_traits 1 \
                    --memory_cfg "${RECOMMENDER_MEM_CFG}" \
                    --class_recommender "${CLASS_RECOMMENDER}" \
                    --nr_threads ${nth} \
                    --cap_limit_MB_idx_0 ${lim}
            fi
        done
    done
done

# exit without error if this point has been reached
exit 0