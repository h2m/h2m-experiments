#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
EXEC_CMD=${EXEC_CMD:-$1}

if [[ -z "${EXEC_CMD}" ]]; then
    echo "ERROR: Parameter EXEC_CMD is not set."
    exit 2
fi

RESULT_DIR_VTUNE=${RESULT_DIR_VTUNE:-"$(pwd)/results_vtune"}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}
RESULT_NUMBER=${RESULT_NUMBER:-"000"}
OMP_NUM_THREADS=${OMP_NUM_THREADS:-16}

# create result directory
mkdir -p ${RESULT_DIR_VTUNE}
cd ${RESULT_DIR_VTUNE}

# ========================================
# === Load module
# ========================================
module load VTune

# ========================================
# === Execution / profiling
# ========================================
# echo "Running serial version"
# OMP_NUM_THREADS=1 \
# /cvmfs/software.hpc.rwth.de/Linux/RH8/x86_64/intel/skylake_avx512/software/VTune/2023.0.0/vtune/2023.0.0/bin64/vtune -collect hotspots -result-dir=${RESULT_DIR_VTUNE}/r${RESULT_NUMBER}hs_serial -- \
#     numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} /usr/bin/time -v \
#         ${EXEC_CMD} &> ${RESULT_DIR_VTUNE}/output_vtune_serial.log

echo "Running multi-threaded version"
/cvmfs/software.hpc.rwth.de/Linux/RH8/x86_64/intel/skylake_avx512/software/VTune/2023.0.0/vtune/2023.0.0/bin64/vtune -collect hotspots -result-dir=${RESULT_DIR_VTUNE}/r${RESULT_NUMBER}hs_${OMP_NUM_THREADS}-threads -- \
    numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} /usr/bin/time -v \
        ${EXEC_CMD} &> ${RESULT_DIR_VTUNE}/output_vtune_${OMP_NUM_THREADS}-threads.log

# exit without error if this point has been reached
exit 0