#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
# get directory of current script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

RESULT_DIR_H2M=${RESULT_DIR_H2M:-$1}
CLASS_METRICS=${CLASS_METRICS:-$2}
EVALUATION_SCRIPT="${SCRIPT_DIR}/../evaluation/evaluate_results.py"

if [[ -z "${RESULT_DIR_H2M}" ]]; then
    echo "ERROR: Parameter RESULT_DIR_H2M is not set."
    exit 2
fi
if [[ -z "${CLASS_METRICS}" ]]; then
    echo "ERROR: Parameter CLASS_METRICS is not set."
    exit 2
fi

# ==================================================
# Generate statistics once
# ==================================================
python3 ${EVALUATION_SCRIPT} --class_metrics ${CLASS_METRICS} --source ${RESULT_DIR_H2M}

# exit without error if this point has been reached
exit 0