#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
EXEC_CMD=${EXEC_CMD:-$1}
JSON_DIR=${JSON_DIR:-$2}

if [[ -z "${EXEC_CMD}" ]]; then
    echo "ERROR: Parameter EXEC_CMD is not set."
    exit 2
fi

if [[ -z "${JSON_DIR}" ]]; then
    echo "ERROR: Parameter JSON_DIR is not set."
    exit 2
fi

APPLY_DATE_POSTFIX=${APPLY_DATE_POSTFIX:-0}
RESULT_DIR_H2M=${RESULT_DIR_H2M:-"$(pwd)/$(date +"%Y-%m-%d_%H%M%S")_results_h2m-interception"}
N_REPS=${N_REPS:-1}
CAP_LIMITS_MB=${CAP_LIMITS_MB:-"500,750,1000"}
N_THREADS=${N_THREADS:-"16"}
OBJ_FNC_VARIANTS=${OBJ_FNC_VARIANTS:-"GAIN_AND_TRANSFER_TIME,GAIN"}
PM_WEIGHTING_FACTORS=${PM_WEIGHTING_FACTORS:-"1"}
UPCOMING_PH=${UPCOMING_PH:-"1,2,3,all"}
EXECUTE_PH=1

if [[ "${APPLY_DATE_POSTFIX}" == "1" ]] ; then
    RESULT_DIR_H2M="${RESULT_DIR_H2M}_$(date +"%Y-%m-%d_%H%M%S")"
fi

if [[ "${UPCOMING_PH}" == "none" ]] ; then
    export UPCOMING_PH=""
    EXECUTE_PH=0
fi

# create result directory
mkdir -p ${RESULT_DIR_H2M}
cd ${RESULT_DIR_H2M}

# apply split to retrieve arrays
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_CAP_LIMITS_MB <<< "${CAP_LIMITS_MB}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_OBJ_FNC_VARIANTS <<< "${OBJ_FNC_VARIANTS}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_N_THREADS <<< "${N_THREADS}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_UPCOMING_PH <<< "${UPCOMING_PH}"
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_PM_WEIGHTING_FACTORS <<< "${PM_WEIGHTING_FACTORS}"

# ========================================
# === Environment variables for execution
# ========================================
export OMP_PLACES=cores
export OMP_PROC_BIND=close

export H2M_PRINT_STATISTICS=1
export H2M_PRINT_CONFIG_VALUES=1
export H2M_PRINT_AFFINITY_MASKS=0
export H2M_PRINT_PHASE_TIMES=1
export H2M_PRINT_CALLSTACK_OFFSETS=0
export H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES=2000000
export H2M_VERBOSITY_LVL=27
export H2M_BACKGROUND_THREAD_ENABLED=0
export H2M_BACKGROUND_THREAD_PIN_MODE=1
export H2M_BACKGROUND_THREAD_PIN_CORE=17
export H2M_MIGRATION_ENABLED=0
export H2M_NUM_MIGRATION_THREADS=1
export H2M_JSON_USE_ID=0
export H2M_PRINT_JSON_ALLOCATION_WARNINGS=1

export FLEXMALLOC_DEFINITIONS=${SCRIPT_DIR}/../../configs/flexmalloc_allocators.cfg
export FLEXMALLOC_LOCATIONS=${SCRIPT_DIR}/../../configs/flexmalloc_locations.cfg
export FLEXMALLOC_DEBUG=0
export FLEXMALLOC_VERBOSE=0
export FLEXMALLOC_GDB=0
export FLEXMALLOC_IGNORE_LOCATIONS_ON_FALLBACK_ALLOCATOR=0
export FLEXMALLOC_SOURCE_FRAMES=true
export FLEXMALLOC_READ_OFFSET_BASE=10
export FLEXMALLOC_FALLBACK_ALLOCATOR=h2m/any
export FLEXMALLOC_MINSIZE_THRESHOLD=100000
export FLEXMALLOC_MINSIZE_THRESHOLD_ALLOCATOR=posix
CMD_PREFIX="stdbuf -oL flexmalloc.sh ${FLEXMALLOC_DEFINITIONS} ${FLEXMALLOC_LOCATIONS}"

# ========================================
# === Execution
# ========================================
for nthr in "${FINAL_N_THREADS[@]}"
do
    echo "=== Execution with ${nthr} Threads"
    export OMP_NUM_THREADS=${nthr}

    # ========================================
    # === References: Initial data placement
    # === Lower and upper bound
    # ========================================
    unset H2M_MAX_MEM_CAP_OVERALL_MB_HBW
    unset H2M_DEFAULT_MEM_SPACE
    unset H2M_JSON_TRAIT_FILE

    for rep in $(seq 1 1 $((${N_REPS})))
    do
        cur_name="output_idp_0MB_rep${rep}_thr${nthr}"
        cur_res_path="${RESULT_DIR_H2M}/${cur_name}"

        # upper, lower bound
        echo "Running idp experiment HBW-only"
        eval "H2M_FORCED_ALLOC_MEM_SPACE=HBW ${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path}_hbw.log status=none
        echo "Running idp experiment LARGE_CAP-only"
        eval "H2M_FORCED_ALLOC_MEM_SPACE=LARGE_CAP ${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path}_lcap.log status=none
    done

    # ========================================
    # === Runs with capacity limitations
    # ========================================
    for lim in "${FINAL_CAP_LIMITS_MB[@]}"
    do
        # restrict HBW memory to a certain amount of MB
        export H2M_MAX_MEM_CAP_OVERALL_MB_HBW=$((${lim}*101/100))

        for rep in $(seq 1 1 $((${N_REPS})))
        do
            cur_name_idp="output_idp_${lim}MB_rep${rep}_thr${nthr}"
            cur_name_pbdp="output_pbdp_${lim}MB_rep${rep}_thr${nthr}"
            cur_res_path_idp="${RESULT_DIR_H2M}/${cur_name_idp}"
            cur_res_path_pbdp="${RESULT_DIR_H2M}/${cur_name_pbdp}"

            # =============================================
            # === Initial Data Placement using FCFS
            # =============================================
            unset H2M_JSON_TRAIT_FILE
            export H2M_DEFAULT_MEM_SPACE="HBW"
            # H2M base variant: only prescriptive traits are considered. So only execute it once
            echo "Running experiment for ${cur_name_idp}_h2m-fcfs"
            eval "${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path_idp}_h2m-fcfs.log status=none

            # From now on: default memory allocation in LARGE_CAP (usually used as fallback)
            export H2M_DEFAULT_MEM_SPACE="LARGE_CAP"

            for variant in "${FINAL_OBJ_FNC_VARIANTS[@]}"
            do
                var_clean=${variant//"_"/"-"}
                # =============================================
                # === Initial Data Placement using JSON traits
                # =============================================
                export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_complete-program_${nthr}-threads_pmwf-1_${lim}MB_variant-${variant}.json"
                echo "Running experiment for ${cur_name_idp}_h2m-complete-${var_clean}"
                eval "${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path_idp}_h2m-complete-${var_clean}-pmwf-1.log status=none

                # =============================================
                # === Initial Data Placement using JSON traits
                # === Considering allocation lifetime
                # =============================================
                export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_complete-program-lifetime_${nthr}-threads_pmwf-1_${lim}MB_variant-${variant}.json"
                echo "Running experiment for ${cur_name_idp}_h2m-complete-lifetime-${var_clean}"
                eval "${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path_idp}_h2m-complete-lifetime-${var_clean}-pmwf-1.log status=none

                # =============================================
                # === Phase-based Placement using JSON traits
                # =============================================
                if [[ "${EXECUTE_PH}" == "1" ]] ; then
                    for ucp in "${FINAL_UPCOMING_PH[@]}"
                    do
                        if [[ "${variant}" == "GAIN_AND_TRANSFER_TIME" || "${variant}" == "GAIN_AND_TRANSFER_TIME_DECAY" ]] ; then
                            for pmwf in "${FINAL_PM_WEIGHTING_FACTORS[@]}"
                            do
                                export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_per-phase_${nthr}-threads_pmwf-${pmwf}_${lim}MB_variant-${variant}_upcoming_${ucp}.json"
                                echo "Running experiment for ${cur_name_pbdp}_h2m-upcoming-${ucp}-${var_clean}-pmwf-${pmwf}"
                                eval "H2M_MIGRATION_ENABLED=1 ${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path_pbdp}_h2m-upcoming-${ucp}-${var_clean}-pmwf-${pmwf}.log status=none
                            done
                        else
                            export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_per-phase_${nthr}-threads_pmwf-1_${lim}MB_variant-${variant}_upcoming_${ucp}.json"
                            echo "Running experiment for ${cur_name_pbdp}_h2m-upcoming-${ucp}-${var_clean}-pmwf-1"
                            eval "H2M_MIGRATION_ENABLED=1 ${CMD_PREFIX} ${EXEC_CMD}" |& dd of=${cur_res_path_pbdp}_h2m-upcoming-${ucp}-${var_clean}-pmwf-1.log status=none
                        fi
                    done
                fi

                # =============================================
                # === Phase-based Placement using partial prefetching
                # =============================================
                # export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_per-phase_${nthr}-threads_${lim}MB_variant-${variant}_lookahead.json"
                # echo "Running experiment for ${cur_name_pbdp}_h2m-lookahead-${var_clean}"
                # H2M_BACKGROUND_THREAD_ENABLED=1 H2M_MIGRATION_ENABLED=1 ${CMD_PREFIX} ${EXEC_CMD} |& dd of=${cur_res_path_pbdp}_h2m-lookahead-${var_clean}.log status=none
            done
        done
    done
done

# exit without error if this point has been reached
exit 0
