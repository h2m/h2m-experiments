#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
EXEC_APP=${EXEC_APP:-$1}
EXEC_CMD=${EXEC_CMD:-$2}

if [[ -z "${EXEC_APP}" ]]; then
    echo "ERROR: Parameter EXEC_APP is not set."
    exit 2
fi

if [[ -z "${EXEC_CMD}" ]]; then
    echo "ERROR: Parameter EXEC_CMD is not set."
    exit 2
fi

RESULT_DIR_GPROF=${RESULT_DIR_GPROF:-"$(pwd)/results_gprof"}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}
OMP_NUM_THREADS=${OMP_NUM_THREADS:-16}

# create result directory
mkdir -p ${RESULT_DIR_GPROF}

# ========================================
# === Execution / profiling
# ========================================
# echo "Running serial version"
# OMP_NUM_THREADS=1 \
# numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} /usr/bin/time -v \
#     ${EXEC_CMD} &> ${RESULT_DIR_GPROF}/output_gprof_serial.log
# gprof ${EXEC_APP} gmon.out &> ${RESULT_DIR_GPROF}/analysis_gprof_serial.log

echo "Running multi-threaded version"
numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} /usr/bin/time -v \
    ${EXEC_CMD} &> ${RESULT_DIR_GPROF}/output_gprof_${OMP_NUM_THREADS}-threads.log
gprof ${EXEC_APP} gmon.out &> ${RESULT_DIR_GPROF}/analysis_gprof_${OMP_NUM_THREADS}-threads.log

# exit without error if this point has been reached
exit 0