#!/bin/zsh

RESULT_ROOT=${RESULT_ROOT:-"$(pwd)"}
VALUES_SAMPLING_RATE=${VALUES_SAMPLING_RATE:-"6000"}

if [[ -z "${RESULT_ROOT}" ]]; then
    echo "ERROR: Parameter RESULT_ROOT is not set."
    exit 2
fi
if [[ -z "${VALUES_SAMPLING_RATE}" ]]; then
    echo "ERROR: Parameter VALUES_SAMPLING_RATE is not set."
    exit 2
fi

export RESULT_DIR_PLAIN=${RESULT_ROOT}/results_plain
export RESULT_DIR_VTUNE=${RESULT_ROOT}/results_vtune
export RESULT_DIR_GPROF=${RESULT_ROOT}/results_gprof
export RESULT_PREFIX_NUMAMMA=${RESULT_ROOT}/results_numamma
export RESULT_DIR_NUMAMMA=${RESULT_PREFIX_NUMAMMA}_sr_${VALUES_SAMPLING_RATE}
export RESULT_DIR_H2M=${RESULT_ROOT}/results_h2m_interception
export SPEC_DATA_ROOT=${RESULT_DIR_NUMAMMA}
export JSON_DIR=${RESULT_DIR_NUMAMMA}/recommendations