#!/bin/zsh

# get current script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ -z "${NPB_ROOT}" ]]; then
    echo "ERROR: Parameter NPB_ROOT is not set."
    exit 2
fi

# benchmark specific settings (build & run)
export NPB_BENCHMARK="cg"
export NPB_CLASS="D"
export EXEC_APP="${NPB_ROOT}/bin/${NPB_BENCHMARK}.${NPB_CLASS}.x"
export EXEC_CMD="${EXEC_APP}"
# export CUSTOM_FLAGS=${CUSTOM_FLAGS:-"-pg"} # used for gprof

# extend result root with benchmark info
export RESULT_ROOT="${RESULT_ROOT}_${NPB_BENCHMARK}_CLASS-${NPB_CLASS}"

# threading specific settings
export N_THREADS=${N_THREADS:-16}
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-${N_THREADS}}
export OMP_PLACES=${OMP_PLACES:-cores}
export OMP_PROC_BIND=${OMP_PROC_BIND:-close}

# NumaMMA & optimizer settings
export VALUES_SAMPLING_RATE="6000"
# Note for CLASS B: Maximum resident set size (kbytes): 357,564
# Note for CLASS C: Maximum resident set size (kbytes): 919,808
# Note for CLASS D: Maximum resident set size (kbytes): 17,116,548
# Note for CLASS E: Maximum resident set size (kbytes): ??????
export CAP_LIMITS_MB="200,500,750,1000,1500,2000,4000,6500"
export OBJ_FNC_VARIANTS="GAIN_AND_TRANSFER_TIME,GAIN,AGGR_LATENCY"
export KS_SAMPLING_FREQ=${VALUES_SAMPLING_RATE}
export KS_ARR_NEXT_N_PHASES="1,2"
export UPCOMING_PH="1,2"

# set directories dynamically based on root
source ${SCRIPT_DIR}/../generic/set_dynamic_dirs.sh

# first build application or benchmark
${SCRIPT_DIR}/run_build.sh || exit 1

# run workflow steps
# ${SCRIPT_DIR}/../generic/run_vtune.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_gprof.sh || exit 1
${SCRIPT_DIR}/../generic/run_plain.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_numamma.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_optimizer.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_h2m_interception.sh || exit 1
