#!/bin/zsh

# get current script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ -z "${NPB_ROOT}" ]]; then
    echo "ERROR: Parameter NPB_ROOT is not set."
    exit 2
fi

# benchmark specific settings (build & run)
export NPB_BENCHMARK="lu"
export NPB_CLASS="D"
export EXEC_APP="${NPB_ROOT}/bin/${NPB_BENCHMARK}.${NPB_CLASS}.x"
export EXEC_CMD="${EXEC_APP}"
# export CUSTOM_FLAGS=${CUSTOM_FLAGS:-"-pg"} # used for gprof

# extend result root with benchmark info
export RESULT_ROOT="${RESULT_ROOT}_${NPB_BENCHMARK}_CLASS-${NPB_CLASS}"

# threading specific settings
export N_THREADS=${N_THREADS:-16}
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-${N_THREADS}}
export OMP_PLACES=${OMP_PLACES:-cores}
export OMP_PROC_BIND=${OMP_PROC_BIND:-close}

# NumaMMA & optimizer settings
export VALUES_SAMPLING_RATE="6000"
# Note for CLASS B: Maximum resident set size (kbytes): 152,328
# Note for CLASS C: Maximum resident set size (kbytes): 581,200
# Note for CLASS D: Maximum resident set size (kbytes): 9,079,940
# Note for CLASS E: Maximum resident set size (kbytes): 141,255,804
export CAP_LIMITS_MB="1816,2270,2996,3632,4540,6810" # 20%,25%,33%,40%,50%,75% of max resident size
export OBJ_FNC_VARIANTS="GAIN_AND_TRANSFER_TIME,GAIN"
export KS_SAMPLING_FREQ=${VALUES_SAMPLING_RATE}
export KS_ARR_NEXT_N_PHASES="1,2"
export UPCOMING_PH="1,2"
export N_REPS=${N_REPS:-1}
export CLASS_METRICS="CMetricsNPB"

# set directories dynamically based on root
source ${SCRIPT_DIR}/../generic/set_dynamic_dirs.sh

# first build application or benchmark
${SCRIPT_DIR}/run_build.sh || exit 1

# run workflow steps
# ${SCRIPT_DIR}/../generic/run_vtune.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_gprof.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_plain.sh || exit 1
${SCRIPT_DIR}/../generic/run_numamma.sh || exit 1
${SCRIPT_DIR}/../generic/run_optimizer.sh || exit 1
${SCRIPT_DIR}/../generic/run_h2m_interception.sh || exit 1
${SCRIPT_DIR}/../generic/run_evaluation.sh || exit 1
