#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
NPB_ROOT=${NPB_ROOT:-$1}
NPB_BENCHMARK=${NPB_BENCHMARK:-$2}
NPB_CLASS=${NPB_CLASS:-$3}

if [[ -z "${NPB_ROOT}" ]]; then
    echo "ERROR: Parameter NPB_ROOT is not set."
    exit 2
fi

if [[ -z "${NPB_BENCHMARK}" ]]; then
    echo "ERROR: Parameter NPB_BENCHMARK is not set."
    exit 2
fi

if [[ -z "${NPB_CLASS}" ]]; then
    echo "ERROR: Parameter NPB_CLASS is not set."
    exit 2
fi

if [[ -z "${H2M_INCLUDE_DIR}" ]]; then
    # Required for running NPB makefiles
    echo "ERROR: Variable H2M_INCLUDE_DIR is not set"
    exit 2
fi

export CUSTOM_FLAGS=${CUSTOM_FLAGS:-"-g"}
export RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-1}

# move to NPB-OMP directory
cd ${NPB_ROOT}

# make sure both print files are executable
chmod u+x sys/print_header sys/print_instructions

# prepare Makefile configuration according to your needs (Might require setting compiler variables etc.)
cp config/make.def.h2m config/make.def

# create bin directory required for the build
mkdir -p bin

# build a desired benchmark
make ${NPB_BENCHMARK} CLASS=${NPB_CLASS}

# exit without error if this point has been reached
exit 0