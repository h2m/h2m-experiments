#!/bin/zsh
# ========================================
# === load modules
# ========================================
VER_GCC=10.3.0
module purge
module load GCC/${VER_GCC}
module load numamma/icelake-gcc-${VER_GCC}
module load h2m/gcc-10.3.0
module load hwloc/2.7.0_gcc

# ========================================
# === directiries & tools
# ========================================
# remember current directory
CUR_DIR=$(pwd)
APP_DIR=/work/jk869269/repos/hpc-projects/h2m/applications/graph500_openmp/graph500reference

# ========================================
# === build
# ========================================
make -C ${APP_DIR} clean
make -C ${APP_DIR}

# ========================================
# === environment variables
# ========================================
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export OMP_NUM_THREADS=12

# VALUES_ALARM=(10 100 200)
# VALUES_SAMPLING_RATE=(1000 10000)
# Note: 
#   Sampling rate of 1000 is more than enough
#   Test revealed that it highly depends whether per-thread buffers run full.
#   For now alarm (collecting samples) of 100 is good.
VALUES_ALARM=(100)
VALUES_SAMPLING_RATE=(1000)
REF_MEM_DOMAIN=0

# ========================================
# === Execution / profiling
# ========================================

for val_al in "${VALUES_ALARM[@]}"
do
    for val_sr in "${VALUES_SAMPLING_RATE[@]}"
    do
        echo "Running experiment with alarm=${val_al} and sampling_rate=${val_sr}"

        # create directory
        OUT_NAME=${CUR_DIR}/output_numamma_alarm_${val_al}_sr_${val_sr}
        RES_DIR=${CUR_DIR}/results_numamma_alarm_${val_al}_sr_${val_sr}
        mkdir -p ${RES_DIR}
        export H2M_DUMP_DIR=${RES_DIR}

        # execute
        numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} \
            numamma --alarm=${val_al} --sampling-rate=${val_sr} --buffer-size=512 -d -D --outputdir=/${RES_DIR} -- \
            ${APP_DIR}/omp-csr/omp-csr -s 18 -e 512 &> ${OUT_NAME}.log
    done
done
