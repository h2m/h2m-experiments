#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
APP_DIR=${APP_DIR:-$1}

if [[ -z "${APP_DIR}" ]]; then
    echo "ERROR: Parameter APP_DIR is not set."
    exit 2
fi

export CUSTOM_FLAGS=${CUSTOM_FLAGS:-""}
export RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-1}

# ========================================
# === Build
# ========================================
EXE_NAME="lulesh2.0"
make -C ${APP_DIR} LULESH_EXEC=${EXE_NAME} clean
make -C ${APP_DIR} LULESH_EXEC=${EXE_NAME} OPTIMIZE=1 DEBUG=1

# exit without error if this point has been reached
exit 0
