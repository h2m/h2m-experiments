#!/bin/zsh

# get current script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ -z "${APP_DIR}" ]]; then
    echo "ERROR: Parameter APP_DIR is not set."
    exit 2
fi

# benchmark specific settings (build & run)
export EXEC_APP="${APP_DIR}/lulesh2.0"
export EXEC_CMD="${EXEC_APP} -i 10 -s 200"
# export CUSTOM_FLAGS=${CUSTOM_FLAGS:-"-pg"} # used for gprof

# threading specific settings
export N_THREADS=${N_THREADS:-16}
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-${N_THREADS}}
export OMP_PLACES=${OMP_PLACES:-cores}
export OMP_PROC_BIND=${OMP_PROC_BIND:-close}

# NumaMMA & optimizer settings
export VALUES_SAMPLING_RATE="2000"
# Note: Maximum resident set size (kbytes): 7,217,380
export CAP_LIMITS_MB="1443,1804,2382,2887,3609,5413" # 20%,25%,33%,40%,50%,75% of max resident size
export OBJ_FNC_VARIANTS="GAIN_AND_TRANSFER_TIME,GAIN_AND_TRANSFER_TIME_DECAY"
export KS_SAMPLING_FREQ=${VALUES_SAMPLING_RATE}
export KS_ARR_NEXT_N_PHASES="1,2,3,all"
export UPCOMING_PH="1,2,3,all"
export PM_WEIGHTING_FACTORS="1,5,10,20"
export N_REPS=${N_REPS:-1}
export CLASS_METRICS="CMetricsLULESH"

# set directories dynamically based on root
source ${SCRIPT_DIR}/../generic/set_dynamic_dirs.sh

# first build application or benchmark
${SCRIPT_DIR}/run_build.sh || exit 1

# run workflow steps
# ${SCRIPT_DIR}/../generic/run_vtune.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_gprof.sh || exit 1
# ${SCRIPT_DIR}/../generic/run_plain.sh || exit 1
${SCRIPT_DIR}/../generic/run_numamma.sh || exit 1
${SCRIPT_DIR}/../generic/run_optimizer.sh || exit 1
${SCRIPT_DIR}/../generic/run_h2m_interception.sh || exit 1
${SCRIPT_DIR}/../generic/run_evaluation.sh || exit 1
