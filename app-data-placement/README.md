# Workflow

## 1. Prerequisites
- Loaded desired compiler such as GCC
- Software for H2M workflow compiled (with desired compiler) and loaded
  - H2M runtime
  - NumaMMA and potential dependencies
  - FlexMalloc with H2M extensions

## 2. Executing workflow for benchmarks/applications
- For most applications, we can then apply a unified generic workflow provided by the following scripts in folder `generic`

| Script | Description |
|--------|-------------|
| `set_dynamic_dirs.sh` | Based on a `RESULT_ROOT` folder, defines additional source and result folder paths that are used by the individual workflow scripts |
| `run_vtune.sh` | (Optional) Executes program with Intel VTune |
| `run_gprof.sh` | (Optional) Executes program and retrieves a gprof flat profile |
| `run_plain.sh` | (Optional) Executes plain program but gives addition information with `/usr/bin/time -v` such as max. resident size, which can be used to determine the right capacity limitation for the subsequent steps |
| `run_numamma.sh` | Executes program and apply memory access profiling using NumaMMA |
| `run_optimizer.sh` | Based on the NumaMMA dumps, generates JSON files with placement optimizations for subsequent executions with H2M |
| `run_h2m_interception.sh` | Executes program with allocation interception using FlexMalloc and H2M. In that step, JSON files that were created during the placement optimization step are used |

- Each of the benchmarks / applications has 1 or more `main` file, which sets the necessary environment variables required by the scripts and calls individual workflow scripts. These can be commented/uncommented as needed.
- **Exceptions**
  - Each benchmark or application has a custom `run_build.sh` located in the corresponding subfolder
  - miniFE has a separate `run_h2m_interception.sh` because it generates additional result files that contain relevant information. As we need to distinguish these files, we need to pass names to individual runs
  - SPEC-OMP2012: Building and running SPEC is much more complicated due to its own separate build and execution system. Therefore, it has custom scripts that have the same purpose but replace benchmark executions with SPEC-specific invocations.

## 3. Examples
The following examples demonstrate what needs to be done to run the workflow for applications.

### 3.1. Common Steps and Settings
There are some common action items and variables that need to be set.

```bash
export H2M_REPO_ROOT=/work/jk869269/repos/hpc-projects/h2m
export H2M_APP_ROOT=/work/jk869269/repos/hpc-projects/h2m/applications
export H2M_INCLUDE_DIR=/home/jk869269/install/h2m/gcc-11.3.0/include
export RECOMMENDER_SCRIPT=${H2M_REPO_ROOT}/h2m-recommender/scripts/generate_trait_recommendations.py
export RECOMMENDER_MEM_CFG=${H2M_REPO_ROOT}/h2m-recommender/configs/icelake-optane.json
```

### 3.2. multi-phase-app / XSBench / miniFE / LULESH
```bash
# Step 1: switch into benchmark-specific folder (here: XSBench)
cd XSBench

# Step 2: Set user-specific variables
export APP_DIR=${H2M_APP_ROOT}/xsbench/openmp-threading
export RESULT_ROOT=$(pwd)/results

# Step 3: Make sure every script is executable
chmod u+x ./../generic/*.sh ./run_* ./main.sh

# Step 3: Run desired benchmark-specific workflow script
./main.sh
```

### 3.3. NPB
```bash
# Step 1: switch into benchmark-specific folder (here: NPB)
cd NPB

# Step 2: Set user-specific variables
export NPB_ROOT=${H2M_APP_ROOT}/npb/NPB3.4-OMP
export RESULT_ROOT=$(pwd)/results

# Step 3: Make sure every script is executable
chmod u+x ./../generic/*.sh ./run_* ./main*.sh

# Step 3: Run desired benchmark-specific workflow script
./main_BT.sh
./main_SP.sh
```

## 4. Gathering and evaluating H2M runs
- The python script in folder `evaluation` will collect execution times from all log files resulting from individual H2M executions.
- Based on that it will then create CSV summary files and some preliminary plots, that are placed in a subfolder of the result directory
- As outputs for each benchmark are different, there is a corresponding Metric class for each benchmark / suite that tells how to parse the content from the resulting log files

```bash
# switch directory
cd evaluation

# run evaluation script for benchmark (here: XSBench)
python3 evaluate_results.py --class_metrics CMetricsXSBench --source ../XSBench/results/results_h2m_interception/
```

## 5. Troubleshooting

### 5.1. NumaMMA
It may happens that NumaMMA fails launching and stops its execution due to a `SIGALRM` (`14`).
It might be linked to the use of the automated flush of the performance measurements.
In order to deactivate it, you may export in your environment `VALUES_ALARM=disabled`&nbsp;;
`NUMAMMA_BUFFER_SIZE` will likely need to be set as well (by default, `512`) to ensure enough space will be allocated for storing the measurements.
This value is the size in kB, of the memory allocated for storing measurement from NumaMMA, for each thread.
One can also deactivate the automated flush of the results when it is full by exporting `NUMAMMA_FLUSH_FULL_SAMPLING=[0|no|false|disabled]` to their environment.

