#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_NAME=${APP_NAME:-$1}
CFG_NAME=${CFG_NAME:-$2}

if [[ -z "${APP_NAME}" ]]; then
    echo "ERROR: Parameter APP_NAME is not set."
    exit 2
fi
if [[ -z "${CFG_NAME}" ]]; then
    echo "ERROR: Parameter CFG_NAME is not set."
    exit 2
fi

PROBLEM_SIZE=${PROBLEM_SIZE:-"train"}
RESULT_DIR_VTUNE=${RESULT_DIR_VTUNE:-"$(pwd)/results_vtune_${APP_NAME}"}
N_ITERATIONS=${N_ITERATIONS:-1}
N_THREADS=${N_THREADS:-16}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}
GPROF=${GPROF:-0}
DEBUG_VERSION=${DEBUG_VERSION:-1}
RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-0}
RESULT_NUMBER=${RESULT_NUMBER:-"000"}
export SPEC_OMP2012_NO_RUNDIR_DEL=1

# create result directory
mkdir -p ${RESULT_DIR_VTUNE}

# ========================================
# === Load module
# ========================================
module load VTune

# ========================================
# === Execution / profiling
# ========================================
echo "Running serial version"
SUBMIT="/cvmfs/software.hpc.rwth.de/Linux/RH8/x86_64/intel/skylake_avx512/software/VTune/2023.0.0/vtune/2023.0.0/bin64/vtune -collect hotspots -result-dir=${RESULT_DIR_VTUNE}/r${RESULT_NUMBER}hs_serial -- \$command"
runspec --config=${SCRIPT_DIR}/${CFG_NAME} --ignore_errors --define OUTPUT_ROOT=${RESULT_DIR_VTUNE} --define DEBUG_VERSION=${DEBUG_VERSION} --define SUBMIT="${SUBMIT}" --define N_ITERATIONS=${N_ITERATIONS} --size=${PROBLEM_SIZE} --threads=1 ${APP_NAME}

echo "Running multi-threaded version"
SUBMIT="/cvmfs/software.hpc.rwth.de/Linux/RH8/x86_64/intel/skylake_avx512/software/VTune/2023.0.0/vtune/2023.0.0/bin64/vtune -collect hotspots -result-dir=${RESULT_DIR_VTUNE}/r${RESULT_NUMBER}hs_${N_THREADS}-threads -- \$command"
runspec --config=${SCRIPT_DIR}/${CFG_NAME} --ignore_errors --define OUTPUT_ROOT=${RESULT_DIR_VTUNE} --define DEBUG_VERSION=${DEBUG_VERSION} --define SUBMIT="${SUBMIT}" --define N_ITERATIONS=${N_ITERATIONS} --size=${PROBLEM_SIZE} --threads=${N_THREADS} ${APP_NAME}

# exit without error if this point has been reached
exit 0