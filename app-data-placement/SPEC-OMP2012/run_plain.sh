#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_NAME=${APP_NAME:-$1}
CFG_NAME=${CFG_NAME:-$2}

if [[ -z "${APP_NAME}" ]]; then
    echo "ERROR: Parameter APP_NAME is not set."
    exit 2
fi
if [[ -z "${CFG_NAME}" ]]; then
    echo "ERROR: Parameter CFG_NAME is not set."
    exit 2
fi

PROBLEM_SIZE=${PROBLEM_SIZE:-"train"}
RESULT_DIR_PLAIN=${RESULT_DIR_PLAIN:-"$(pwd)/results_plain_${APP_NAME}"}
N_ITERATIONS=${N_ITERATIONS:-1}
N_THREADS=${N_THREADS:-16}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}
GPROF=${GPROF:-0}
DEBUG_VERSION=${DEBUG_VERSION:-0}
RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-0}
export SPEC_OMP2012_NO_RUNDIR_DEL=1

# create result directory
mkdir -p ${RESULT_DIR_PLAIN}

# overwrite submit section
SUBMIT="numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} /usr/bin/time -v \$command"

# ========================================
# === Execution / profiling
# ========================================
echo "Running plain experiment"
runspec --config=${SCRIPT_DIR}/${CFG_NAME} --ignore_errors --define OUTPUT_ROOT=${RESULT_DIR_PLAIN} --define DEBUG_VERSION=${DEBUG_VERSION} --define GPROF=${GPROF} --define RECORD_TRANSITIONS=${RECORD_TRANSITIONS} --define SUBMIT="${SUBMIT}" --define N_ITERATIONS=${N_ITERATIONS} --size=${PROBLEM_SIZE} --threads=${N_THREADS} ${APP_NAME}
