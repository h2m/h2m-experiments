#!/bin/zsh

# get current script directory
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# benchmark specific settings
export APP_NAME=363.swim
export PROBLEM_SIZE=train

# threading specific settings
export N_THREADS=${N_THREADS:-16}
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-${N_THREADS}}
export OMP_PLACES=${OMP_PLACES:-cores}
export OMP_PROC_BIND=${OMP_PROC_BIND:-close}

# NumaMMA & optimizer settings
export VALUES_SAMPLING_RATE="6000"
# Note: Maximum resident set size (kbytes): 1,153,212
export CAP_LIMITS_MB="300,400,500,600,700,800"
export OBJ_FNC_VARIANTS="GAIN_AND_TRANSFER_TIME,GAIN"
export KS_SAMPLING_FREQ=${VALUES_SAMPLING_RATE}
export KS_ARR_NEXT_N_PHASES="1"
export UPCOMING_PH="1"
export N_REPS=${N_REPS:-1}
export CLASS_METRICS="CMetricsSPEC"

# overwrite result root here to incoporate app an problem size
export RESULT_ROOT="${RESULT_ROOT}_${APP_NAME}_${PROBLEM_SIZE}"

# set directories dynamically based on root
source ${SCRIPT_DIR}/../generic/set_dynamic_dirs.sh

# run workflow steps
# ${SCRIPT_DIR}/run_vtune.sh || exit 1
# ${SCRIPT_DIR}/run_gprof.sh || exit 1
# ${SCRIPT_DIR}/run_plain.sh || exit 1
${SCRIPT_DIR}/run_numamma.sh || exit 1
${SCRIPT_DIR}/../generic/run_optimizer.sh || exit 1
${SCRIPT_DIR}/run_h2m_interception.sh || exit 1
${SCRIPT_DIR}/../generic/run_evaluation.sh || exit 1
