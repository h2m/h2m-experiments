# SPEC OMP2012 Workflow

## Prerequisites
- Loaded desired compiler such as GCC
- Software for H2M workflow compiled with desired compiler and loaded
  - H2M runtime
  - NumaMMA and potential dependencies
  - FlexMalloc with H2M extensions
- SPEC OMP2012 suite downloaded and "installed"
- Sourced the `shrc` file in the SPEC root folder to be able to execute `runspec`
    ```bash
    # first define the path to the SPEC main directory
    export SPEC_DIR=path/to/SPEC
    # now source the file
    cd ${SPEC_DIR}
    source shrc
    ```

## Executing individual steps/scripts
- Set user specific variables required by the script

## Executing complete workflow
```bash
# Step 1: Set user-specific variables
export H2M_INCLUDE_DIR=/home/jk869269/install/h2m/gcc-11.3.0/include
export RECOMMENDER_SCRIPT=/work/jk869269/repos/hpc-projects/h2m/h2m-recommender/scripts/generate_trait_recommendations.py
export RECOMMENDER_MEM_CFG=/work/jk869269/repos/hpc-projects/h2m/h2m-recommender/configs/icelake-optane.json
export CFG_NAME=rwth-optane-gcc.cfg
export RESULT_ROOT=$(pwd)/results

# Step 2: Run desired benchmark-specific workflow script
./SPEC_351.bwaves.sh
```