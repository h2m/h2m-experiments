#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_NAME=${APP_NAME:-$1}
CFG_NAME=${CFG_NAME:-$2}

if [[ -z "${APP_NAME}" ]]; then
    echo "ERROR: Parameter APP_NAME is not set."
    exit 2
fi
if [[ -z "${CFG_NAME}" ]]; then
    echo "ERROR: Parameter CFG_NAME is not set."
    exit 2
fi

PROBLEM_SIZE=${PROBLEM_SIZE:-"train"}
RESULT_PREFIX_NUMAMMA=${RESULT_PREFIX_NUMAMMA:-"$(pwd)/results_numamma_${APP_NAME}"}
N_ITERATIONS=${N_ITERATIONS:-1}
N_THREADS=${N_THREADS:-16}
REF_MEM_DOMAIN=${REF_MEM_DOMAIN:-0}
GPROF=${GPROF:-0}
DEBUG_VERSION=${DEBUG_VERSION:-0}
RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-1}
export SPEC_OMP2012_NO_RUNDIR_DEL=1

# Note:
#   Sampling rate of 1000 is more than enough
#   Test revealed that it highly depends whether per-thread buffers run full.
#   For now alarm (collecting samples) of 100 is good.
VALUES_SAMPLING_RATE=${VALUES_SAMPLING_RATE:-"6000"}

NUMAMMA_BUFFER_SIZE=${NUMAMMA_BUFFER_SIZE:-"2048"}
case "`sed 'y/[A-Z]/[a-z]/' <<< ${NUMAMMA_FLUSH_FULL_SAMPLING}`" in
    0|no|disabled|false)
        NUMAMMA_FLUSH_FULL_SAMPLING="--flush=no"
        ;;
esac

# apply split to retrieve arrays
IFS=',' read -r"${BASH_VERSION:+a}${ZSH_VERSION:+A}" FINAL_VALUES_SAMPLING_RATE <<< "${VALUES_SAMPLING_RATE}"

# ========================================
# === Execution / profiling
# ========================================

for val_sr in "${FINAL_VALUES_SAMPLING_RATE[@]}"
do
    echo "Running experiment with sampling_rate=${val_sr}"

    # create directory
    RES_DIR=${RESULT_PREFIX_NUMAMMA}_sr_${val_sr}
    mkdir -p ${RES_DIR}
    cd ${RES_DIR}
    export H2M_DUMP_DIR=${RES_DIR}
    OUT_NAME=${RES_DIR}/output_numamma.log

    SUBMIT="numactl --cpunodebind 0 --membind ${REF_MEM_DOMAIN} numamma --sampling-rate=${val_sr} --buffer-size=${NUMAMMA_BUFFER_SIZE} ${NUMAMMA_FLUSH_FULL_SAMPLING} -d -D --outputdir=${RES_DIR} -- \$command &> ${OUT_NAME}"
    # execute
    runspec --config=${SCRIPT_DIR}/${CFG_NAME} --ignore_errors --define OUTPUT_ROOT=${RES_DIR} --define RECORD_TRANSITIONS=${RECORD_TRANSITIONS} --define SUBMIT="${SUBMIT}" --size=${PROBLEM_SIZE} --threads=${N_THREADS} --define REDIRECT=1 ${APP_NAME} \
        || exit $?
done

# exit without error if this point has been reached
exit 0
