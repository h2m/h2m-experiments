# Info on Buffers
with "large" we have the following buffer/callsite sizes:
| name | size |
|------|-----:|
| nuclide_grid | 192.603120 MB |
| index_grid | 5697.842320 MB |
| unionized_energy_array | 32.100520 MB |
