#!/bin/zsh
# ========================================
# === Directiries & tools
# ========================================
APP_DIR=${APP_DIR:-$1}
JSON_DIR=${JSON_DIR:-$2}
TOOL_DIR=${TOOL_DIR:-$3}

TOOL_PATH_PRIO_SEN=${TOOL_DIR}/commit-strategy_priority_sensitivity/tool.so

# create result directory
RESULT_DIR="$(pwd)/$(date +"%Y-%m-%d_%H%M%S")_results_h2m"
mkdir -p ${RESULT_DIR}

# ========================================
# === Environment variables
# ========================================
export OMP_PLACES=cores
export OMP_PROC_BIND=close
export H2M_PRINT_STATISTICS=0
export H2M_PRINT_CONFIG_VALUES=1
export H2M_VERBOSITY_LVL=25
export H2M_BACKGROUND_THREAD_ENABLED=0
export H2M_MIGRATION_ENABLED=0
PROG_NAME=XSBench
N_REPS=1
ENV_TRAITS_VALS=(3) # 1: NumaMMA (hard-coded), 2: ASan, 3: NumaMMA (from JSON file)
CAP_LIMITS_MB=(200 500 750 1000 1500 2000 4000 6500)
N_THREADS=(12)

# ========================================
# === Build versions
# ========================================
# Version H2M default (no traits)
program="${PROG_NAME}" make -C ${APP_DIR} clean
program="${PROG_NAME}" USE_H2M_ALLOC_ABSTRACTION=1 SPLIT_ALLOCATIONS=1 LOAD_TRAIT_VERSION=-1 make -C ${APP_DIR}

# Version H2M for manual placements
program="${PROG_NAME}_manual" make -C ${APP_DIR} clean
program="${PROG_NAME}_manual" USE_H2M_ALLOC_ABSTRACTION=1 SPLIT_ALLOCATIONS=1 LOAD_TRAIT_VERSION=0 make -C ${APP_DIR}

# Versions for different H2M trait variants
for trait_val in "${ENV_TRAITS_VALS[@]}"
do
    program="${PROG_NAME}_tv_${trait_val}" make -C ${APP_DIR} clean
    program="${PROG_NAME}_tv_${trait_val}" USE_H2M_ALLOC_ABSTRACTION=1 SPLIT_ALLOCATIONS=1 LOAD_TRAIT_VERSION=${trait_val} make -C ${APP_DIR}
done

# ========================================
# === Execution
# ========================================
for nthr in "${N_THREADS[@]}"
do
    echo "=== Execution with ${nthr} Threads"

    # specify execution parameters
    EXEC_PARAMS="-t ${nthr} -s large -l 100"

    # ========================================
    # === References: Initial data placement
    # === Lower and upper bound
    # ========================================
    unset H2M_MAX_MEM_CAP_OVERALL_MB_HBW
    unset H2M_DEFAULT_MEM_SPACE
    unset H2M_JSON_TRAIT_FILE

    PROG="${APP_DIR}/${PROG_NAME}"

    for rep in $(seq 1 1 $((${N_REPS})))
    do
        cur_name="output_idp_0MB_rep${rep}_thr${nthr}"
        cur_res_path="${RESULT_DIR}/${cur_name}"

        # upper, lower bound
        echo "Running idp experiment HBW-only"
        H2M_FORCED_ALLOC_MEM_SPACE=HBW       ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_hbw.log
        echo "Running idp experiment LARGE_CAP-only"
        H2M_FORCED_ALLOC_MEM_SPACE=LARGE_CAP ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_lcap.log
    done

    # ========================================
    # === Manual data placements
    # === - only a single data item in HBW
    # === - rest goes to LARGE_CAP
    # ========================================
    # set default memory space if not specifed otherwise
    export H2M_DEFAULT_MEM_SPACE=LARGE_CAP
    PROG="${APP_DIR}/${PROG_NAME}_manual"

    for rep in $(seq 1 1 $((${N_REPS})))
    do
        cur_name="output_idp_0MB_rep${rep}_thr${nthr}"
        cur_res_path="${RESULT_DIR}/${cur_name}"

        echo "Running idp experiment NUCLIDE=HBW"
        MEMSPACE_NUCLIDE=HBW ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_nuclide-hbw.log
        echo "Running idp experiment UNI=HBW"
        MEMSPACE_UNI=HBW     ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_uni-hbw.log
        echo "Running idp experiment IDX=HBW"
        MEMSPACE_IDX=HBW     ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_idx-hbw.log
    done

    # ========================================
    # === First-come-first-served
    # ========================================
    # try to allocate in faster BW memory as long as possible
    export H2M_DEFAULT_MEM_SPACE=HBW
    PROG="${APP_DIR}/${PROG_NAME}"

    for lim in "${CAP_LIMITS_MB[@]}"
    do
        # restrict HBW memory to a certain amount of MB
        export H2M_MAX_MEM_CAP_OVERALL_MB_HBW=$((${lim}*1.05))

        for rep in $(seq 1 1 $((${N_REPS})))
        do
            cur_name="output_idp_${lim}MB_rep${rep}_thr${nthr}"
            cur_res_path="${RESULT_DIR}/${cur_name}"

            echo "Running idp experiment ${cur_name}_h2m-fcfs"
            ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_h2m-fcfs.log
        done
    done

    # ========================================
    # === Initial data placement experiments
    # ========================================
    # From now on: default memory allocation in LARGE_CAP (usually used as fallback)
    export H2M_DEFAULT_MEM_SPACE="LARGE_CAP"

    for trait_val in "${ENV_TRAITS_VALS[@]}"
    do
        # use exe corresponding to trait variant
        PROG="${APP_DIR}/${PROG_NAME}_tv_${trait_val}"

        if [ "${trait_val}" = "1" ]; then
            trait_version="numamma"
        elif [ "${trait_val}" = "3" ]; then
            trait_version="json"
        else
            trait_version="asan"
        fi

        for lim in "${CAP_LIMITS_MB[@]}"
        do
            # restrict HBW memory to a certain amount of MB
            export H2M_MAX_MEM_CAP_OVERALL_MB_HBW=$((${lim}*1.05))
            export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_complete-program_${nthr}-threads_${lim}MB_variant-GAIN.json"

            for rep in $(seq 1 1 $((${N_REPS})))
            do
                cur_name="output_idp_${lim}MB_rep${rep}_thr${nthr}"
                cur_res_path="${RESULT_DIR}/${cur_name}"

                # traits coming from JSON file based on NumaMMA profile
                if [ "${trait_val}" = "3" ]; then
                    # H2M JSON variant -> Getting traits / placement decision directly from JSON file
                    echo "Running experiment for ${cur_name}_h2m-json"
                    ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_h2m-json.log
                fi

                # H2M with custom heuristics based on NumaMMA profile
                if [ "${trait_val}" = "1" ]; then
                    echo "Running experiment for ${cur_name}_h2m-prio"
                    H2M_TOOL_LIBRARIES=${TOOL_PATH_PRIO_SEN} ${PROG} ${EXEC_PARAMS} &> ${cur_res_path}_h2m-prio-sensitivity.log
                fi
            done
        done
    done
done
