#!/bin/zsh
# ========================================
# === Parameters and settings
# ========================================
APP_DIR=${APP_DIR:-$1}

if [[ -z "${APP_DIR}" ]]; then
    echo "ERROR: Parameter APP_DIR is not set."
    exit 2
fi

export CUSTOM_FLAGS=${CUSTOM_FLAGS:-"-g"}
export RECORD_TRANSITIONS=${RECORD_TRANSITIONS:-1}

# ========================================
# === Build
# ========================================
make -C ${APP_DIR} -f Makefile.gnu.openmp clean
make -C ${APP_DIR} -f Makefile.gnu.openmp all

# exit without error if this point has been reached
exit 0