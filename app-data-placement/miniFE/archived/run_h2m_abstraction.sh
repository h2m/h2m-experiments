#!/bin/zsh
# ========================================
# === Directiries & tools
# ========================================
APP_DIR=${APP_DIR:-$1}
JSON_DIR=${JSON_DIR:-$2}

# create result directory
RESULT_DIR="$(pwd)/$(date +"%Y-%m-%d_%H%M%S")_results_h2m-abstraction"
mkdir -p ${RESULT_DIR}

# ========================================
# === Build
# ========================================
export EXE_NAME="miniFE_abstraction.x"
make -C ${APP_DIR} -f Makefile.gnu.openmp clean
make -C ${APP_DIR} -f Makefile.gnu.openmp USE_H2M_ALLOC_ABSTRACTION=1 all

# ========================================
# === JSON IDs
# ========================================
export ID_MAT_ROWS=5
export ID_MAT_ROW_OFFSET=12
export ID_MAT_PACKED_COL=9
export ID_MAT_PACKED_COEFF=10
export ID_VEC_B=13
export ID_VEC_X=14
export ID_VEC_R=2
export ID_VEC_P=3
export ID_VEC_AP=4

# ========================================
# === Environment variables
# ========================================
# OpenMP specific
export OMP_PLACES=cores
export OMP_PROC_BIND=close

# H2M specific
export H2M_PRINT_STATISTICS=1
export H2M_PRINT_CONFIG_VALUES=1
export H2M_PRINT_AFFINITY_MASKS=1
export H2M_VERBOSITY_LVL=29
export H2M_BACKGROUND_THREAD_ENABLED=0
export H2M_BACKGROUND_THREAD_PIN_MODE=1
export H2M_BACKGROUND_THREAD_PIN_CORE=17
export H2M_MIGRATION_ENABLED=1
export H2M_NUM_MIGRATION_THREADS=1

# CAP_LIMITS_MB=(250 500 750 1000 1500 1750 2000 2500)
CAP_LIMITS_MB=(750)
N_THREADS=(16)
N_REPS=1
PROG="${APP_DIR}/${EXE_NAME}"
EXEC_PARAMS="nx=192"

# Heuristic specific
UPCOMING_PH=(1 2 "all")
OBJ_FNC_VARIANTS=("GAIN_AND_TRANSFER_TIME" "GAIN" "AGGR_LATENCY")

# ========================================
# === Execution
# ========================================
cd ${RESULT_DIR} # required to place miniFE result files there as well
for nthr in "${N_THREADS[@]}"
do
    echo "=== Execution with ${nthr} Threads"
    export OMP_NUM_THREADS=${nthr}

    # ========================================
    # === References: Initial data placement
    # === Lower and upper bound
    # ========================================
    unset H2M_MAX_MEM_CAP_OVERALL_MB_HBW
    unset H2M_DEFAULT_MEM_SPACE
    for rep in $(seq 1 1 $((${N_REPS})))
    do
        cur_name="output_idp_0MB_rep${rep}_thr${nthr}"
        cur_res_path="${RESULT_DIR}/${cur_name}"

        # upper, lower bound
        echo "Running experiment for ${cur_name}_hbw"
        H2M_MIGRATION_ENABLED=0 H2M_FORCED_ALLOC_MEM_SPACE=HBW       ${PROG} ${EXEC_PARAMS} name="hbw" &> ${cur_res_path}_hbw.log
        echo "Running experiment for ${cur_name}_lcap"
        H2M_MIGRATION_ENABLED=0 H2M_FORCED_ALLOC_MEM_SPACE=LARGE_CAP ${PROG} ${EXEC_PARAMS} name="lcap" &> ${cur_res_path}_lcap.log
    done

    # ========================================
    # === Runs with capacity limitations
    # ========================================
    for lim in "${CAP_LIMITS_MB[@]}"
    do
        # restrict HBW memory to a certain amount of MB
        export H2M_MAX_MEM_CAP_OVERALL_MB_HBW=$((${lim}*1.05))

        for rep in $(seq 1 1 $((${N_REPS})))
        do
            cur_name_idp="output_idp_${lim}MB_rep${rep}_thr${nthr}"
            cur_name_pbdp="output_pbdp_${lim}MB_rep${rep}_thr${nthr}"
            cur_res_path_idp="${RESULT_DIR}/${cur_name_idp}"
            cur_res_path_pbdp="${RESULT_DIR}/${cur_name_pbdp}"

            # =============================================
            # === Initial Data Placement using FCFS
            # =============================================
            unset H2M_JSON_TRAIT_FILE
        	export H2M_DEFAULT_MEM_SPACE="HBW"
            # H2M base variant: only prescriptive traits are considered. So only execute it once
            echo "Running experiment for ${cur_name_idp}_h2m-fcfs"
            H2M_MIGRATION_ENABLED=0 ${PROG} ${EXEC_PARAMS} name="lim-${lim}MB_fcfs" &> ${cur_res_path_idp}_h2m-fcfs.log

            # From now on: default memory allocation in LARGE_CAP (usually used as fallback)
            export H2M_DEFAULT_MEM_SPACE="LARGE_CAP"

            for variant in "${OBJ_FNC_VARIANTS[@]}"
            do
                var_clean=${variant//"_"/"-"}
                # =============================================
                # === Initial Data Placement using JSON traits
                # =============================================
                export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_complete-program_${nthr}-threads_${lim}MB_variant-${variant}.json"
                echo "Running experiment for ${cur_name_idp}_h2m-complete-${var_clean}"
                H2M_MIGRATION_ENABLED=0 ${PROG} ${EXEC_PARAMS} name="lim-${lim}MB_h2m-complete-${var_clean}" &> ${cur_res_path_idp}_h2m-complete-${var_clean}.log

                # =============================================
                # === Phase-based Placement using JSON traits
                # =============================================
                for ucp in "${UPCOMING_PH[@]}"
                do
                    export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_per-phase_${nthr}-threads_${lim}MB_variant-${variant}_upcoming_${ucp}.json"
                    echo "Running experiment for ${cur_name_pbdp}_h2m-upcoming-${ucp}-${var_clean}"
                    H2M_MIGRATION_ENABLED=1 ${PROG} ${EXEC_PARAMS} name="lim-${lim}MB_h2m-pbdp-upcoming-${ucp}-${var_clean}" &> ${cur_res_path_pbdp}_h2m-upcoming-${ucp}-${var_clean}.log
                done

                # =============================================
                # === Phase-based Placement using partial prefetching
                # =============================================
                export H2M_JSON_TRAIT_FILE="${JSON_DIR}/traits_per-phase_${nthr}-threads_${lim}MB_variant-${variant}_lookahead.json"
                echo "Running experiment for ${cur_name_pbdp}_h2m-lookahead-${var_clean}"
                H2M_BACKGROUND_THREAD_ENABLED=1 H2M_MIGRATION_ENABLED=1 ${PROG} ${EXEC_PARAMS} name="lim-${lim}MB_h2m-pbdp-lookahead-${var_clean}" &> ${cur_res_path_pbdp}_h2m-lookahead-${var_clean}.log
            done
        done
    done
done
