# Experiment for miniFE
**Note:**
- Be aware that currently allocation/callsite IDs are hardcoded in miniFE.
- NumaMMA might assign different IDs to allocations if executed again or code is changed/recompiled.
- IDs generated JSON configs and IDs used in code need to match. You might need to revise IDs there.