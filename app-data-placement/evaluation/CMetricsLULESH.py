from CMetricsBase import *

class CMetricsLULESH(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        with open(path) as f: lines = [x.strip() for x in list(f)]
        meas.time_data_init = 1 # dummy because not available
        for line in lines:
            if "Elapsed time" in line:
                tmp_split = line.split("=")
                tmp_split = tmp_split[1].split("(")
                meas.time_work_phase = float(tmp_split[0].strip())
                continue
        return True