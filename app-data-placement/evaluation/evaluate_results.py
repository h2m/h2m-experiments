import argparse
import os, sys
import numpy as np
import statistics as st
import matplotlib.pyplot as plt
import csv

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),"..","..","common")))

from PlotFunctions import *
from CMeasurement import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parses and summarizes execution time measurement results")
    parser.add_argument("--source",         required=True,  type=str, metavar="<folder path>", help=f"source folder containing outputs")
    parser.add_argument("--destination",    required=False, type=str, metavar="<folder path>", default=None, help=f"destination folder where resulting data and plots will be stored")
    parser.add_argument("--class_metrics",  required=False, type=str, metavar="<class_name>", default="CMetricsMultiPhase", help=f"File name / class name for the interface implementation for loading metrics")
    args = parser.parse_args()

    # ========== DEBUG ==========
    # args.source = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m\\00_data\\data-h2m-experiments\\multi-phase-app\\2024-03-22_Optane_Data_for_Clement\\2024-03-21_154620_results_h2m-interception"
    # args.source = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m\\h2m-experiments\\paper-experiments\\XSBench\\2022-10-13_170452_results_h2m_KNL"
    # args.source = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m\\h2m-experiments\\initial-data-placement\\multi-phase-app\\2023-06-07_135856_results_h2m"
    # ========== DEBUG ==========

    if not os.path.exists(args.source):
        print(f"Source folder path \"{args.source}\" does not exist")
        sys.exit(1)

     # save results in source folder
    if args.destination is None:
        args.destination = os.path.join(args.source, "result_evaluation")

    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
    
    target_folder_plot = os.path.join(args.destination, "plots")
    if not os.path.exists(target_folder_plot):
        os.makedirs(target_folder_plot)

    # dynamically import the desired metric loader class
    exec(f"from {args.class_metrics} import {args.class_metrics}")

    list_files: list[CMeasurement] = []
    for file in os.listdir(args.source):
        if file.endswith(".log") and "output_" in file:
            cur_file_path = os.path.join(args.source, file)
            print(cur_file_path)

            # read file meta data and measurements
            meas = CMeasurement(cur_file_path, file)
            metr_loader = eval(f"{args.class_metrics}()")
            metr_loader.load_metrics(cur_file_path, meas)
            # append list of measurements
            list_files.append(meas)

    # ========================================
    # === Group: For each number of threads
    # ========================================
    target_file_di   = os.path.join(args.destination, f"data_per-thread_time_data-init.csv")
    target_file_work = os.path.join(args.destination, f"data_per-thread_time_work.csv")
    unique_n_threads = sorted(list(set([x.n_threads for x in list_files])))

    for nthr in unique_n_threads:
        print(f"Generating views for {nthr} Threads ...")
        # filter by number threads
        sub1 = [x for x in list_files if x.n_threads == nthr]

        # retrieve references
        f_hbw   = [x for x in sub1 if x.variant_full == "HBW"]
        f_lcap  = [x for x in sub1 if x.variant_full == "LARGE_CAP"]
        t_data_init_hbw  = st.mean([x.time_data_init for x in f_hbw])
        t_work_hbw       = st.mean([x.time_work_phase for x in f_hbw])
        t_data_init_lcap = st.mean([x.time_data_init for x in f_lcap])
        t_work_lcap      = st.mean([x.time_work_phase for x in f_lcap])

        # get unique combinations
        unique_limits   = sorted(list(set([x.limitMB for x in sub1 if x.limitMB != 0 and x.limitMB != float("inf")])))
        unique_versions = sorted(list(set([x.variant_full for x in sub1])))

        # ========================================
        # === Group: View for versions/limits
        # ========================================
        with open(target_file_di, mode="a", newline='') as f_di:
            writer_di = csv.writer(f_di, delimiter=';')
            with open(target_file_work, mode="a", newline='') as f_work:
                writer_work = csv.writer(f_work, delimiter=';')

                # seprator
                writer_di.writerow([f"===== {nthr} Threads ====="])
                writer_work.writerow([f"===== {nthr} Threads ====="])

                # write header
                header = ['Versions/Cap-Limit HBW']
                for i in range(len(unique_limits)):
                    header.append(f"{int(unique_limits[i])} MB")
                writer_di.writerow(header)
                writer_work.writerow(header)

                map_vals_di         = {}
                map_vals_work       = {}
                arr_versions        = []
                arr_data_di         = []
                arr_data_work       = []
                arr_speedup_di      = []
                arr_speedup_work    = []

                for cur_ver in unique_versions:
                    sub2          = [x for x in sub1 if x.variant_full == cur_ver]
                    tmp_data_di   = [np.nan for _ in range(len(unique_limits))]
                    tmp_data_work = [np.nan for _ in range(len(unique_limits))]

                    # reference versions only executed once
                    if cur_ver == "HBW":
                        tmp_data_di   = [t_data_init_hbw for x in range(len(unique_limits))]
                        tmp_data_work = [t_work_hbw for x in range(len(unique_limits))]
                    elif cur_ver == "LARGE_CAP":
                        tmp_data_di   = [t_data_init_lcap for x in range(len(unique_limits))]
                        tmp_data_work = [t_work_lcap for x in range(len(unique_limits))]
                    else:
                        for i in range(len(unique_limits)):
                            cur_lim          = unique_limits[i]
                            sub3             = [x for x in sub2 if x.limitMB == cur_lim]
                            tmp_data_di[i]   = st.mean([x.time_data_init for x in sub3])
                            tmp_data_work[i] = st.mean([x.time_work_phase for x in sub3])

                    # write to csv
                    writer_di.writerow([cur_ver] + tmp_data_di)
                    writer_work.writerow([cur_ver] + tmp_data_work)
                    # remember assignment
                    map_vals_di[cur_ver]   = tmp_data_di
                    map_vals_work[cur_ver] = tmp_data_work
                    arr_versions.append(cur_ver)
                    arr_data_di.append(tmp_data_di)
                    arr_data_work.append(tmp_data_work)

                # separator rows
                writer_di.writerow([])
                writer_work.writerow([])

                # write header again for speedups
                writer_di.writerow(header)
                writer_work.writerow(header)

                # print speedups
                for cur_ver in unique_versions:
                    tmp_s_di    = [map_vals_di["LARGE_CAP"][x] / map_vals_di[cur_ver][x] for x in range(len(map_vals_di[cur_ver]))]
                    tmp_s_work  = [map_vals_work["LARGE_CAP"][x] / map_vals_work[cur_ver][x] for x in range(len(map_vals_work[cur_ver]))]
                    # write to csv
                    writer_di.writerow([f"Speedup_{cur_ver}"] + tmp_s_di)
                    writer_work.writerow([f"Speedup_{cur_ver}"] + tmp_s_work)

                    arr_speedup_di.append(tmp_s_di)
                    arr_speedup_work.append(tmp_s_work)
                
                # separator rows
                writer_di.writerow([])
                writer_work.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_time_data-init_{nthr}threads.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_limits, arr_versions, arr_data_di, f"{nthr} Threads - Avg execution time for data initialization", "Cap-Limit HBW [MB]", "Execution time [sec]")

                tmp_target_file_name = f"plot_time_work_{nthr}threads.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_limits, arr_versions, arr_data_work, f"{nthr} Threads - Avg execution time for work phase", "Cap-Limit HBW [MB]", "Execution time [sec]")

                tmp_target_file_name = f"plot_speedup_data-init_{nthr}threads.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_limits, arr_versions, arr_speedup_di, f"{nthr} Threads - Avg Speedup for data initialization", "Cap-Limit HBW [MB]", "Speedup")

                tmp_target_file_name = f"plot_speedup_work_{nthr}threads.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_limits, arr_versions, arr_speedup_work, f"{nthr} Threads - Avg Speedup for work phase", "Cap-Limit HBW [MB]", "Speedup")

    # ========================================
    # === Group: For each capacity limit
    # ========================================
    target_file_di   = os.path.join(args.destination, f"data_per-limit_time_data-init.csv")
    target_file_work = os.path.join(args.destination, f"data_per-limit_time_work.csv")
    unique_limits    = sorted(list(set([x.limitMB for x in list_files if x.limitMB != 0 and x.limitMB != float("inf")])))

    for lim in unique_limits:
        print(f"Generating views for Cap-Limit {int(lim)} MB ...")
        # filter by number threads
        sub1 = [x for x in list_files if x.limitMB == lim]

        # get unique combinations
        unique_versions  = ['HBW','LARGE_CAP'] + sorted(list(set([x.variant_full for x in sub1])))
        unique_n_threads = sorted(list(set([x.n_threads for x in sub1])))

        ref_data_init_hbw  = {}
        ref_data_work_hbw  = {}
        ref_data_init_lcap = {}
        ref_data_work_lcap = {}

        # retrieve references
        for nthr in unique_n_threads:        
            f_hbw   = [x for x in list_files if x.variant_full == "HBW" and x.n_threads == nthr]
            f_lcap  = [x for x in list_files if x.variant_full == "LARGE_CAP" and x.n_threads == nthr]
            ref_data_init_hbw[nthr]  = st.mean([x.time_data_init for x in f_hbw])
            ref_data_work_hbw[nthr]  = st.mean([x.time_work_phase for x in f_hbw])
            ref_data_init_lcap[nthr] = st.mean([x.time_data_init for x in f_lcap])
            ref_data_work_lcap[nthr] = st.mean([x.time_work_phase for x in f_lcap])

        # ========================================
        # === Group: View for versions/nthreads
        # ========================================
        with open(target_file_di, mode="a", newline='') as f_di:
            writer_di = csv.writer(f_di, delimiter=';')
            with open(target_file_work, mode="a", newline='') as f_work:
                writer_work = csv.writer(f_work, delimiter=';') 

                # seprator
                writer_di.writerow([f"===== {int(lim)} MB Cap-Limit HBW ====="])
                writer_work.writerow([f"===== {int(lim)} MB Cap-Limit HBW ====="])

                # write header
                header = ['Versions/NrThreads']
                for i in range(len(unique_n_threads)):
                    header.append(f"{unique_n_threads[i]}")
                writer_di.writerow(header)
                writer_work.writerow(header)

                map_vals_di         = {}
                map_vals_work       = {}
                arr_versions        = []
                arr_data_di         = []
                arr_data_work       = []
                arr_speedup_di      = []
                arr_speedup_work    = []

                for cur_ver in unique_versions:
                    sub2          = [x for x in sub1 if x.variant_full == cur_ver]
                    tmp_data_di   = [np.nan for _ in range(len(unique_n_threads))]
                    tmp_data_work = [np.nan for _ in range(len(unique_n_threads))]

                    # reference versions only executed once
                    if cur_ver == "HBW":
                        tmp_data_di   = [ref_data_init_hbw[x] for x in unique_n_threads]
                        tmp_data_work = [ref_data_work_hbw[x] for x in unique_n_threads]
                    elif cur_ver == "LARGE_CAP":
                        tmp_data_di   = [ref_data_init_lcap[x] for x in unique_n_threads]
                        tmp_data_work = [ref_data_work_lcap[x] for x in unique_n_threads]
                    else:
                        for i in range(len(unique_n_threads)):
                            cur_nthr          = unique_n_threads[i]
                            sub3             = [x for x in sub2 if x.n_threads == cur_nthr]
                            tmp_data_di[i]   = st.mean([x.time_data_init for x in sub3])
                            tmp_data_work[i] = st.mean([x.time_work_phase for x in sub3])

                    # write to csv
                    writer_di.writerow([cur_ver] + tmp_data_di)
                    writer_work.writerow([cur_ver] + tmp_data_work)
                    # remember assignment
                    map_vals_di[cur_ver]   = tmp_data_di
                    map_vals_work[cur_ver] = tmp_data_work
                    arr_versions.append(cur_ver)
                    arr_data_di.append(tmp_data_di)
                    arr_data_work.append(tmp_data_work)

                # separator rows
                writer_di.writerow([])
                writer_work.writerow([])

                # write header again for speedups
                writer_di.writerow(header)
                writer_work.writerow(header)

                # print speedups
                for cur_ver in unique_versions:
                    tmp_s_di    = [map_vals_di["LARGE_CAP"][x] / map_vals_di[cur_ver][x] for x in range(len(map_vals_di[cur_ver]))]
                    tmp_s_work  = [map_vals_work["LARGE_CAP"][x] / map_vals_work[cur_ver][x] for x in range(len(map_vals_work[cur_ver]))]
                    # write to csv
                    writer_di.writerow([f"Speedup_{cur_ver}"] + tmp_s_di)
                    writer_work.writerow([f"Speedup_{cur_ver}"] + tmp_s_work)

                    arr_speedup_di.append(tmp_s_di)
                    arr_speedup_work.append(tmp_s_work)
                
                # separator rows
                writer_di.writerow([])
                writer_work.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_time_data-init_{int(lim)}MBLimit.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_data_di, f"{lim}MB Cap-Limit - Avg execution time for data initialization", "Nb threads", "Execution time [sec]")

                tmp_target_file_name = f"plot_time_work_{int(lim)}MBLimit.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_data_work, f"{lim}MB Cap-Limit - Avg execution time for work phase", "Nb threads", "Execution time [sec]")

                tmp_target_file_name = f"plot_speedup_data-init_{int(lim)}MBLimit.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_speedup_di, f"{lim}MB Cap-Limit - Avg Speedup for data initialization", "Nb threads", "Speedup")

                tmp_target_file_name = f"plot_speedup_work_{int(lim)}MBLimit.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_speedup_work, f"{lim}MB Cap-Limit - Avg Speedup for work phase", "Nb threads", "Speedup")
