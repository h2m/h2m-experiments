from CMetricsBase import *
import sys, os

class CMetricsNPB(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        # parse file content
        with open(path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Time in seconds =" in line:
                tmp_split = line.split()
                meas.time_work_phase = float(tmp_split[-1])
                return True
        return True