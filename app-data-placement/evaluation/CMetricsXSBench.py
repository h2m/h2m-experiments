from CMetricsBase import *

class CMetricsXSBench(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        with open(path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Data Init:" in line:
                tmp_split = line.split()
                meas.time_data_init = float(tmp_split[2])
                continue
            if "Runtime:" in line:
                tmp_split = line.split()
                meas.time_work_phase = float(tmp_split[1])
                continue
        return True