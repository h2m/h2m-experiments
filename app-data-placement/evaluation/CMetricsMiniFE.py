from CMetricsBase import *
import sys, os

class CMetricsMiniFE(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        # get folder path and name of file
        cur_folder_path, cur_file_name = os.path.split(os.path.abspath(path))
        cur_file_name = cur_file_name[:-4]

        # find miniFE results file
        for file in os.listdir(cur_folder_path):
            if file.startswith("miniFE") and file.endswith(f".{cur_file_name}.yaml"):
                target_path = os.path.join(cur_folder_path, file)
                print(target_path)

                with open(target_path) as f: lines = [x.strip() for x in list(f)]
                for line in lines:
                    if "Total CG Time:" in line:
                        tmp_split = line.split()
                        meas.time_work_phase = float(tmp_split[-1])
                        return True
        return True