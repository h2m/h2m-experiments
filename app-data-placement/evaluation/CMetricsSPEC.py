from CMetricsBase import *
import sys, os

class CMetricsSPEC(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        # get path for spec-results.txt
        spl_path = path[:-4]
        path_results = spl_path + "_spec-results.txt"
        # parse file content
        meas.time_data_init = -1.0 # dummy as it is not separately reported
        with open(path_results) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Workload elapsed time" in line:
                tmp_split = line.split()
                meas.time_work_phase = float(tmp_split[-2])
                return True
        return True