class CMeasurement():

    def __init__(self, file_path, file_name):
        # get file name and split
        file_name = file_name.split(".")[0]
        tmp_split = file_name.split("_")

        self.time_data_init  = -1.0
        self.time_work_phase = -1.0

        self.placement      = tmp_split[1].strip()
        self.limitMB        = float(tmp_split[2][:-2])
        self.repetition     = int(tmp_split[3][3:])
        self.n_threads      = int(tmp_split[4][3:])
        self.variant        = tmp_split[5].strip()
        self.variant_full   = f"{self.placement}-{self.variant}"

        if tmp_split[-1] == "hbw":
            self.variant_full   = "HBW"
        if tmp_split[-1] == "lcap":
            self.variant_full   = "LARGE_CAP"