from CMetricsBase import *

class CMetricsMultiPhase(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        with open(path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Data allocation took" in line:
                tmp_split = line.split()
                meas.time_data_init = float(tmp_split[3])
                continue
            if "Computations took" in line:
                tmp_split = line.split()
                meas.time_work_phase = float(tmp_split[2])
                continue
        return True