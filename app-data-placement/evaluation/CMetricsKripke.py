from CMetricsBase import *
import sys, os

class CMetricsKripke(CMetricsBase):

    def load_metrics(self, path: str, meas: CMeasurement) -> bool:
        # parse file content
        with open(path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "TIMER_DATA" in line:
                tmp_split = line.split(":")
                tmp_split = tmp_split[-1].split(",")
                # Add times of the different phases
                for time in list(tmp_split):
                    meas.time_work_phase += float(time)
                print(meas.time_work_phase)
                return True
        return True
