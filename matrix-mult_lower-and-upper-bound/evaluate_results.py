import argparse
import os, sys
import numpy as np
import statistics as st
import matplotlib.pyplot as plt
import csv

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'common')))
from PlotFunctions import *

class CFileMetaData():

    def __init__(self, file_path, file_name):
        # get file name and split
        file_name = file_name.split(".")[0]
        tmp_split = file_name.split("_")
        
        self.n_tasks    = int(tmp_split[2])
        self.size       = int(tmp_split[4])
        self.type       = tmp_split[5]
        self.n_threads  = int(tmp_split[7])        
        self.repetition = int(tmp_split[9])

        self.time_allocation = 0.0
        self.time_work = 0.0

        with open(file_path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Data allocation took" in line:
                tmp_split = line.split()
                self.time_allocation = float(tmp_split[3].strip())
                continue
            if "Computations took" in line:
                tmp_split = line.split()
                self.time_work = float(tmp_split[2].strip())
                continue

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parses and summarizes execution time measurement results")
    parser.add_argument("-s", "--source",       required=True,  type=str, metavar="<folder path>", help=f"source folder containing outputs")
    parser.add_argument("-d", "--destination",  required=False, type=str, metavar="<folder path>", default=None, help=f"destination folder where resulting data and plots will be stored")
    args = parser.parse_args()

    if not os.path.exists(args.source):
        print(f"Source folder path \"{args.source}\" does not exist")
        sys.exit(1)

     # save results in source folder
    if args.destination is None:
        args.destination = os.path.join(args.source, "result_evaluation")

    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
    
    target_folder_plot = os.path.join(args.destination, "plots")
    if not os.path.exists(target_folder_plot):
        os.makedirs(target_folder_plot)

    list_files = []
    for file in os.listdir(args.source):
        if file.endswith(".log") and "result_task" in file:
            cur_file_path = os.path.join(args.source, file)
            print(cur_file_path)

            # read file meta data
            file_meta = CFileMetaData(cur_file_path, file)
            list_files.append(file_meta)

    # get unique combinations
    unique_n_threads    = sorted(list(set([x.n_threads for x in list_files])))
    unique_types        = sorted(list(set([x.type for x in list_files])))
    unique_sizes        = sorted(list(set([x.size for x in list_files])))
    unique_sizes_label  = [f"{(ms*ms*3*8/1000.0)} KB" for ms in unique_sizes]
    unique_sizes_label_c= [f"{ms}\n{(ms*ms*3*8/1000.0)} KB" for ms in unique_sizes]

    target_file_alloc   = os.path.join(args.destination, f"data_per_matrix_size_allocation.csv")
    target_file_work    = os.path.join(args.destination, f"data_per_matrix_size_work.csv")

    with open(target_file_alloc, mode="w", newline='') as f_alloc:
        writer_alloc = csv.writer(f_alloc, delimiter=';')
        with open(target_file_work, mode="w", newline='') as f_work:
            writer_work = csv.writer(f_work, delimiter=';')

            for idx_ms in range(len(unique_sizes)):
                ms = unique_sizes[idx_ms]
                writer_alloc.writerow(  [ f"========== Matrix Size {ms} ({unique_sizes_label[idx_ms]}) =========="])
                writer_work.writerow(   [ f"========== Matrix Size {ms} ({unique_sizes_label[idx_ms]}) =========="])
                sub = [x for x in list_files if x.size == ms]
                
                # write header
                header = ['Types/Threads']
                for i in range(len(unique_n_threads)):
                    header.append(str(unique_n_threads[i]))
                writer_alloc.writerow(header); writer_work.writerow(header)

                map_vals_allocation = {}
                map_vals_work       = {}
                arr_types           = []
                arr_data_alloc      = []
                arr_data_work       = []
                arr_data_alloc_s    = []
                arr_data_work_s     = []

                for cur_type in unique_types:
                    sub2                = [x for x in sub if x.type == cur_type]
                    tmp_data_allocation = [np.nan for x in range(len(unique_n_threads))]
                    tmp_data_work       = [np.nan for x in range(len(unique_n_threads))]

                    for i in range(len(unique_n_threads)):
                        cur_thr = unique_n_threads[i]
                        sub3    = [x for x in sub2 if x.n_threads == cur_thr]

                        tmp_data_allocation[i]  = st.mean([x.time_allocation for x in sub3])
                        tmp_data_work[i]        = st.mean([x.time_work for x in sub3])

                    # write to csv
                    writer_alloc.writerow([cur_type] + tmp_data_allocation)
                    writer_work.writerow([cur_type] + tmp_data_work)
                    # remember assignment
                    map_vals_allocation[cur_type]   = tmp_data_allocation
                    map_vals_work[cur_type]         = tmp_data_work
                    arr_types.append(cur_type)
                    arr_data_alloc.append(tmp_data_allocation)
                    arr_data_work.append(tmp_data_work)
                
                # separator rows
                writer_alloc.writerow([]); writer_work.writerow([])

                # print speedups
                for cur_type in unique_types:
                    tmp_s_alloc = [map_vals_allocation["large-cap"][x] / map_vals_allocation[cur_type][x] for x in range(len(map_vals_allocation[cur_type]))]
                    tmp_s_work  = [map_vals_work["large-cap"][x] / map_vals_work[cur_type][x] for x in range(len(map_vals_work[cur_type]))]
                    # write to csv
                    writer_alloc.writerow([f"Speedup_{cur_type}"] + tmp_s_alloc)
                    writer_work.writerow([f"Speedup_{cur_type}"] + tmp_s_work)
                    arr_data_alloc_s.append(tmp_s_alloc)
                    arr_data_work_s.append(tmp_s_work)
                
                # separator rows
                writer_alloc.writerow([]); writer_work.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_alloc_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_types, arr_data_alloc, f"Allocation time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Allocation time [sec]")

                tmp_target_file_name = f"plot_work_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_types, arr_data_work, f"Execution time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Execution time [sec]")

                tmp_target_file_name = f"plot_alloc_speedup_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_types, arr_data_alloc_s, f"Speedups allocation time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Speedup")

                tmp_target_file_name = f"plot_work_speedup_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_types, arr_data_work_s, f"Speedups execution time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Speedup")
    
    target_file_alloc   = os.path.join(args.destination, f"data_per_threads_allocation.csv")
    target_file_work    = os.path.join(args.destination, f"data_per_threads_work.csv")

    with open(target_file_alloc, mode="w", newline='') as f_alloc:
        writer_alloc = csv.writer(f_alloc, delimiter=';')
        with open(target_file_work, mode="w", newline='') as f_work:
            writer_work = csv.writer(f_work, delimiter=';')

            for cur_thr in unique_n_threads:
                writer_alloc.writerow([ f"========== # Threads: {cur_thr} =========="])
                writer_work.writerow([  f"========== # Threads: {cur_thr} =========="])
                sub = [x for x in list_files if x.n_threads == cur_thr]
                
                # write header
                header = ['Types/Size']
                for i in range(len(unique_sizes)):
                    ms = unique_sizes[i]
                    header.append(f"{unique_sizes[i]} ({unique_sizes_label[i]})")
                writer_alloc.writerow(header); writer_work.writerow(header)

                map_vals_allocation = {}
                map_vals_work       = {}
                arr_types           = []
                arr_data_alloc      = []
                arr_data_work       = []
                arr_data_alloc_s    = []
                arr_data_work_s     = []

                for cur_type in unique_types:
                    sub2                = [x for x in sub if x.type == cur_type]
                    tmp_data_allocation = [np.nan for x in range(len(unique_sizes))]
                    tmp_data_work       = [np.nan for x in range(len(unique_sizes))]

                    for i in range(len(unique_sizes)):
                        ms                      = unique_sizes[i]
                        sub3                    = [x for x in sub2 if x.size == ms]
                        tmp_data_allocation[i]  = st.mean([x.time_allocation for x in sub3])
                        tmp_data_work[i]        = st.mean([x.time_work for x in sub3])

                    # write to csv
                    writer_alloc.writerow([cur_type] + tmp_data_allocation)
                    writer_work.writerow([cur_type] + tmp_data_work)
                    # remember assignment
                    map_vals_allocation[cur_type]   = tmp_data_allocation
                    map_vals_work[cur_type]         = tmp_data_work
                    arr_types.append(cur_type)
                    arr_data_alloc.append(tmp_data_allocation)
                    arr_data_work.append(tmp_data_work)

                # separator rows
                writer_alloc.writerow([]); writer_work.writerow([])

                # print speedups
                for cur_type in unique_types:
                    tmp_s_alloc = [map_vals_allocation["large-cap"][x] / map_vals_allocation[cur_type][x] for x in range(len(map_vals_allocation[cur_type]))]
                    tmp_s_work  = [map_vals_work["large-cap"][x] / map_vals_work[cur_type][x] for x in range(len(map_vals_work[cur_type]))]
                    # write to csv
                    writer_alloc.writerow([f"Speedup_{cur_type}"] + tmp_s_alloc)
                    writer_work.writerow([f"Speedup_{cur_type}"] + tmp_s_work)
                    arr_data_alloc_s.append(tmp_s_alloc)
                    arr_data_work_s.append(tmp_s_work)
                
                # separator rows
                writer_alloc.writerow([]); writer_work.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_alloc_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_types, arr_data_alloc, f"Allocation time for {cur_thr} threads", "Matrix Size", "Allocation time [sec]")

                tmp_target_file_name = f"plot_work_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_types, arr_data_work, f"Execution time for {cur_thr} threads", "Matrix Size", "Execution time [sec]")

                tmp_target_file_name = f"plot_alloc_speedup_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_types, arr_data_alloc_s, f"Speedups allocation time for {cur_thr} threads", "Matrix Size", "Speedup")

                tmp_target_file_name = f"plot_work_speedup_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_types, arr_data_work_s, f"Speedups execution time for {cur_thr} threads", "Matrix Size", "Speedup")