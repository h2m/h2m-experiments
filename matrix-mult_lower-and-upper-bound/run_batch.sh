#!/usr/local_rwth/bin/zsh
#SBATCH --time=03:00:00
#SBATCH --exclusive
#SBATCH --partition=c18m
#SBATCH --nodes=1

hostname
# source ~/.zshrc
# source env_h2m.sh

# build benchmark once
SCRIPT_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BENCH_DIR="${SCRIPT_DIR}/../../h2m/examples/matrix_mult"
BLOCKED_DGEMM=0 NUM_ITERATIONS=2 NUM_REPETITIONS=1 N_PHASES=4 make release --directory=${BENCH_DIR}

# remember current directory
CUR_DIR=$(pwd)

# set initial postfix
export RESULT_POSTFIX=${RESULT_POSTFIX:-"Optane"}

# ==================================================
# ==== spread with fixed task size (strong scaling)
# ==================================================
export OMP_PROC_BIND=spread
export FIXED_NTASKS=1
export RESULT_DIR="results_${RESULT_POSTFIX}_${OMP_PROC_BIND}_fixed"
mkdir ${RESULT_DIR} && cd ${RESULT_DIR}
zsh ../run_experiment.sh
cd ${CUR_DIR}

# ==================================================
# ==== spread with dynamic task size (weak scaling)
# ==================================================
export OMP_PROC_BIND=spread
export FIXED_NTASKS=0
export RESULT_DIR="results_${RESULT_POSTFIX}_${OMP_PROC_BIND}_dynamic"
mkdir ${RESULT_DIR} && cd ${RESULT_DIR}
zsh ../run_experiment.sh
cd ${CUR_DIR}

# ==================================================
# ==== close with fixed task size (strong scaling)
# ==================================================
export OMP_PROC_BIND=close
export FIXED_NTASKS=1
export RESULT_DIR="results_${RESULT_POSTFIX}_${OMP_PROC_BIND}_fixed"
mkdir ${RESULT_DIR} && cd ${RESULT_DIR}
zsh ../run_experiment.sh
cd ${CUR_DIR}

# ==================================================
# ==== close with dynamic task size (weak scaling)
# ==================================================
export OMP_PROC_BIND=close
export FIXED_NTASKS=0
export RESULT_DIR="results_${RESULT_POSTFIX}_${OMP_PROC_BIND}_dynamic"
mkdir ${RESULT_DIR} && cd ${RESULT_DIR}
zsh ../run_experiment.sh
cd ${CUR_DIR}
