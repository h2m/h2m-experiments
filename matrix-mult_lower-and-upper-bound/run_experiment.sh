#!/usr/local_rwth/bin/zsh

echo "========================================"
echo "=== Matrix Mult - Lower/Upper Bound  ==="
echo "========================================"

# make it possible to pass list of number of threads
# MAT_SIZES_STR=${MAT_SIZES_STR:-"200,400,600,800,1000,1200,1400,1600,1800"}
MAT_SIZES_STR=${MAT_SIZES_STR:-"400,600,800,1400,1800"}
MAT_SIZES=($(echo ${MAT_SIZES_STR} | tr ',' "\n"))
# N_THREADS_STR=${N_THREADS_STR:-"1,2,4,6,8,10,12,14,16,18,20,22,24"}
# N_THREADS_STR=${N_THREADS_STR:-"4,8,12,16,20,24"}
N_THREADS_STR=${N_THREADS_STR:-"1,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32"}
N_THREADS=($(echo ${N_THREADS_STR} | tr ',' "\n"))
N_REP=${N_REP:-2}
FIXED_NTASKS=${FIXED_NTASKS:-1}
export OMP_PLACES=${OMP_PLACES:-"cores"}
export OMP_PROC_BIND=${OMP_PROC_BIND:-"spread"}
# export KMP_AFFINITY=verbose
export H2M_PRINT_CONFIG_VALUES=1

# display hardware overview
numactl -H

# set benchmark directory
SCRIPT_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BENCH_DIR="${SCRIPT_DIR}/../../h2m/examples/matrix_mult"

# run benchmark experiments
for ms in "${MAT_SIZES[@]}"
do
    for n_thr in "${N_THREADS[@]}"
    do
        export OMP_NUM_THREADS=${n_thr}
        if [ "${FIXED_NTASKS}" = "1" ]; then
            export N_TASKS=24
        else
            export N_TASKS=$((n_thr * 2))
        fi

        for rep in {1..${N_REP}}
        do
            echo "Running Size=${ms}, Threads=${n_thr}, OMP_PROC_BIND=${OMP_PROC_BIND}, FIXED_NTASKS=${FIXED_NTASKS}, NTASKS=${N_TASKS}, Rep=${rep} -- LargeCap-only"
            export RES_FILE="result_task_${N_TASKS}_size_${ms}_large-cap_threads_${n_thr}_rep_${rep}.log"
            H2M_MIGRATION_ENABLED=0 H2M_FORCED_ALLOC_MEM_SPACE=LARGE_CAP \
                no_numa_balancing ${BENCH_DIR}/matrix_mult.exe ${N_TASKS} ${ms} &> ${RES_FILE}
            
            echo "Running Size=${ms}, Threads=${n_thr}, OMP_PROC_BIND=${OMP_PROC_BIND}, FIXED_NTASKS=${FIXED_NTASKS}, NTASKS=${N_TASKS}, Rep=${rep} -- HBW-only"
            export RES_FILE="result_task_${N_TASKS}_size_${ms}_hbw_threads_${n_thr}_rep_${rep}.log"
            H2M_MIGRATION_ENABLED=0 H2M_FORCED_ALLOC_MEM_SPACE=HBW \
                no_numa_balancing ${BENCH_DIR}/matrix_mult.exe ${N_TASKS} ${ms} &> ${RES_FILE}
            
            echo "Running Size=${ms}, Threads=${n_thr}, OMP_PROC_BIND=${OMP_PROC_BIND}, FIXED_NTASKS=${FIXED_NTASKS}, NTASKS=${N_TASKS}, Rep=${rep} -- Regular"
            export RES_FILE="result_task_${N_TASKS}_size_${ms}_regular_threads_${n_thr}_rep_${rep}.log"
            H2M_MIGRATION_ENABLED=1 \
                no_numa_balancing ${BENCH_DIR}/matrix_mult.exe ${N_TASKS} ${ms} &> ${RES_FILE}
        done
    done
done
