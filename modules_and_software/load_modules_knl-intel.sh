#!/bin/zsh
# ========================================
# === load modules
# ========================================
module purge
module load DEVELOP
module load gcc/8
module load intel
module load intelmpi
module use ~/.modules
module load hwloc/2.7.0
module load h2m