#!/bin/zsh
# ========================================
# === load modules
# ========================================
module purge
module load iccifort/2019.5.281
module use ~/.modules
module load hwloc/2.7.0
module load h2m