#!/bin/zsh
# ========================================
# === load modules
# ========================================
VER_GCC=11.3.0
module purge
module load GCC/${VER_GCC}
module load hwloc/2.7.1
module load Python/3.10.4
module load Gurobi/10.0.0

# custom modules for software build for the workflow
module use ~/.modules
module load h2m/gcc-${VER_GCC}
module load numamma/gcc-${VER_GCC}
module load flexmalloc/gcc-${VER_GCC}