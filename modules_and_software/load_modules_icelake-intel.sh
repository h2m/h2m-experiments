#!/bin/zsh
# ========================================
# === load modules
# ========================================
VER_INTEL=2023a
module purge
module load intel/${VER_INTEL}
module load hwloc/2.9.1
module load Python/3.11.3
module load Gurobi/11.0.0

# custom modules for software build for the workflow
module use ~/.modules
module load h2m/intel-${VER_INTEL}
module load numamma/intel-${VER_INTEL}
module load flexmalloc/intel-${VER_INTEL}