#!/bin/zsh
# ========================================
# === load modules
# ========================================
module purge
module load GCC     # desired version of GCC (e.g. if system-default version is too old)
module load Clang   # load clang
module load CMake   # load CMake
module unload hwloc

# make sure that CC and CXX are set to use Clang compiler
export CC=clang
export CXX=clang++

# ========================================
# === build
# ========================================
git clone https://gitlab.inria.fr/h2m/h2m-access-pattern-detection.git
cd h2m-access-pattern-detection

# define directories
export H2M_MP_DIR_SRC=$(pwd)/src
export H2M_MP_DIR_BUILD=${H2M_MP_DIR_SRC}/BUILD
export H2M_MP_DIR_INSTALL=${H2M_MP_DIR_SRC}/INSTALL

# create BUILD and INSTALL directory
mkdir -p ${H2M_MP_DIR_BUILD}
mkdir -p ${H2M_MP_DIR_INSTALL}
cd ${H2M_MP_DIR_BUILD}

# build the library
cmake -DCMAKE_INSTALL_PREFIX=${H2M_MP_DIR_INSTALL} -DCMAKE_BUILD_TYPE=Release ..
make install
