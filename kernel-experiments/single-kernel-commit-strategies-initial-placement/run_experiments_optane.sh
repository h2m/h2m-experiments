#!/usr/local_rwth/bin/zsh

# ========================================
# Variables
# ========================================
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )" # get path of current script
BIN_DIR=${BIN_DIR:-"${CUR_DIR}/binaries_optane"}
RES_DIR=${RES_DIR:-"${CUR_DIR}/$(date +"%Y-%m-%d_%H%M%S")_results_optane"}

TOOLS_DIR="/work/jk869269/repos/hpc-projects/h2m/h2m/examples/tools"
MEM_SCALE_FACTORS=(12 22)   # To be divided by 10 (I don't trust bash's floating points...)

echo "#============================================="
echo "#=== Directories"
echo "#============================================="
echo "Script Dir: ${CUR_DIR}"
echo "Bin Dir: ${BIN_DIR}"
echo "Result Dir: ${RES_DIR}"

# create result directory if not existing
for tmp_dir in "${MEM_SCALE_FACTORS[@]}"
do
    mkdir -p "${RES_DIR}/$tmp_dir"
done

run_kernel_experiments_for_3mat () {
    for tmp_kernel in "${KERNEL_NAMES[@]}"
    do
        for tmp_size in "${M_SIZES[@]}"
        do
            # exported values are passed by application
            export SIZE_M=${tmp_size}
            export SIZE_N=${tmp_size}
            export SIZE_K=${tmp_size}

            for tmp_rep in "${N_REPS[@]}"
            do
                for tmp_stride in "${STRIDES[@]}"
                do
                    if [[ "${tmp_stride}" = "-1" ]]
                    then
                        ext_stride_name=""
                    else
                        ext_stride_name="_stride_${tmp_stride}"
                    fi

                    tmp_base_exe_name="${tmp_kernel}${ext_stride_name}_reps_${tmp_rep}"
                    tmp_base_name="${tmp_kernel}${ext_stride_name}_ms_${tmp_size}_reps_${tmp_rep}"

                    for tmp_thr in "${N_THREADS[@]}"
                    do
                        export OMP_NUM_THREADS=${tmp_thr}
                        tmp_base_res_name="result_thr_${tmp_thr}_${tmp_base_name}"
                        tmp_base_res_path="${RES_DIR}/${tmp_base_res_name}"
                        echo "Running experiments for ${tmp_base_res_name}"

                        # # baseline
                        # EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline.exe"
                        # ${EXE_NAME} &> "${tmp_base_res_path}_baseline.log"

                        # # ALL=HBW
                        # EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline-ALL-HBW.exe"
                        # ${EXE_NAME} &> "${tmp_base_res_path}_baseline-ALL-HBW.log"

                        for tmp_scale in "${MEM_SCALE_FACTORS[@]}"
                        do
                            tmp_max_mem="$((1+$tmp_size*$tmp_size*8*$tmp_scale/10/1000/1000))"
                            tmp_base_res_path="${RES_DIR}/${tmp_scale}/${tmp_base_res_name}"

                            # FIFO
                            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
                            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="$tmp_max_mem" \
                            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="$tmp_max_mem" \
                                ${EXE_NAME} &> "${tmp_base_res_path}_FIFO.log"

                            # SIZE
                            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
                            H2M_TOOL_LIBRARIES="${TOOLS_DIR}/commit-strategy_size/tool.so" \
                            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="$tmp_max_mem" \
                            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="$tmp_max_mem" \
                                ${EXE_NAME} &> "${tmp_base_res_path}_SIZE.log"

                            # PRIORITY
                            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
                            H2M_TOOL_LIBRARIES="${TOOLS_DIR}/commit-strategy_priority_sensitivity/tool.so" \
                            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="$tmp_max_mem" \
                            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="$tmp_max_mem" \
                                ${EXE_NAME} &> "${tmp_base_res_path}_PRIORITY.log"
                        done
                    done
                done
            done
        done
    done
}

echo "#============================================="
echo "#=== Setting global environment variables"
echo "#============================================="
export PLACEMENT_CHECK=0
export H2M_MIGRATION_ENABLED=0
export H2M_BACKGROUND_THREAD_ENABLED=0
export H2M_PRINT_CONFIG_VALUES=1
export N_ITERS=3
export OMP_PLACES=cores
export OMP_PROC_BIND=close

echo "#============================================="
echo "#=== Run MxNxK Kernels"
echo "#============================================="
KERNEL_NAMES=(kernel_MxNxK kernel_MxNxK_transposedB)
M_SIZES=(4500)
STRIDES=(-1)
N_REPS=(1)
N_THREADS=(16)

run_kernel_experiments_for_3mat

echo "#============================================="
echo "#=== Run dot prod Kernels"
echo "#============================================="
KERNEL_NAMES=(kernel_dot_prod kernel_dot_prod_transposedB)
M_SIZES=(30000)
STRIDES=(1)
N_REPS=(1)
N_THREADS=(16)

run_kernel_experiments_for_3mat
