#!/usr/local_rwth/bin/zsh

# ========================================
# Variables
# ========================================
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )" # get path of current script
SRC_DIR=/work/jk869269/repos/hpc-projects/h2m/temporary-micro-benchmarks/applications/run-single-kernel
BIN_DIR=${BIN_DIR:-"${CUR_DIR}/binaries_optane"}

echo "#============================================="
echo "#=== Directories"
echo "#============================================="
echo "Script Dir: ${CUR_DIR}"
echo "Source Dir: ${SRC_DIR}"
echo "Bin Dir: ${BIN_DIR}"

# create result directory if not existing
mkdir -p ${BIN_DIR}

build_kernels_for_3mat () {
    for tmp_kernel in "${KERNEL_NAMES[@]}"
    do
        for tmp_rep in "${N_REPS[@]}"
        do
            for tmp_stride in "${STRIDES[@]}"
            do
                if [[ "${tmp_stride}" = "-1" ]]
                then
                    ext_stride_name=""
                    ext_stride_params=""
                else
                    ext_stride_name="_stride_${tmp_stride}"
                    ext_stride_params="-DSTRIDE=${tmp_stride}"
                fi

                tmp_base_exe_name="${tmp_kernel}${ext_stride_name}_reps_${tmp_rep}"
                tmp_base_params="-DN_REPS=${tmp_rep} ${ext_stride_params}"

                # # baseline
                # EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline.exe" DC_NAME=data_container_3mat KERNEL_NAME=${tmp_kernel} \
                # COMPILE_FLAGS="${tmp_base_params} -DDO_BASELINE=1" make -C ${SRC_DIR}

                # # ALL=HBW
                # EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline-ALL-HBW.exe" DC_NAME=data_container_3mat KERNEL_NAME=${tmp_kernel} \
                # COMPILE_FLAGS="${tmp_base_params} -DDO_BASELINE=1 -DMEM_SPACE_A=HBW -DMEM_SPACE_B=HBW -DMEM_SPACE_C=HBW" make -C ${SRC_DIR}

                # FIFO / SIZE / PRIORITY
                EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe" DC_NAME=data_container_3mat KERNEL_NAME=${tmp_kernel} \
                COMPILE_FLAGS="${tmp_base_params}" make -C ${SRC_DIR}
            done
        done
    done
}

echo "#============================================="
echo "#=== Build MxMxK Kernels"
echo "#============================================="
KERNEL_NAMES=(kernel_MxNxK kernel_MxNxK_transposedB)
STRIDES=(-1)
N_REPS=(2)
build_kernels_for_3mat

echo "#============================================="
echo "#=== Build dot prod (similar to STREAM Triad)"
echo "#============================================="
KERNEL_NAMES=(kernel_dot_prod kernel_dot_prod_transposedB)
STRIDES=(1)
N_REPS=(2)
build_kernels_for_3mat
