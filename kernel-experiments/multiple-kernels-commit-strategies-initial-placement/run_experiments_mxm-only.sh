#!/usr/local_rwth/bin/zsh

# ========================================
# Variables
# ========================================
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )" # get path of current script
BIN_DIR=${BIN_DIR:-"${CUR_DIR}/binaries"}
RES_DIR=${RES_DIR:-"${CUR_DIR}/$(date +"%Y-%m-%d_%H%M%S")_results"}
TOOLS_DIR="/work/jk869269/repos/hpc-projects/h2m/h2m/examples/tools"

echo "#============================================="
echo "#=== Directories"
echo "#============================================="
echo "Script Dir: ${CUR_DIR}"
echo "Bin Dir: ${BIN_DIR}"
echo "Result Dir: ${RES_DIR}"

# create result directory if not existing
mkdir -p ${RES_DIR}

echo "#============================================="
echo "#=== Setting global environment variables"
echo "#============================================="
export PLACEMENT_CHECK=0
export H2M_MIGRATION_ENABLED=0
export H2M_BACKGROUND_THREAD_ENABLED=0
export H2M_PRINT_CONFIG_VALUES=1
export N_ITERS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close
tmp_base_exe_name="run-multiple-kernels"

echo "#============================================="
echo "#=== Run MxNxK Kernels"
echo "#============================================="
export BASE_SIZE_MB_MATRIX_MXM=24
N_MXM_KERNELS=(5 6 8 10)
N_STEPS=5
N_THREADS=(2 4 8 12 16 20)

for n_ker in "${N_MXM_KERNELS[@]}"
do
    tmp_base_name="n-mxm_${n_ker}"
    size_mb_used="$((${n_ker}*(${n_ker}+1)/2*3*${BASE_SIZE_MB_MATRIX_MXM}))"

    for tmp_thr in "${N_THREADS[@]}"
    do
        export OMP_NUM_THREADS=${tmp_thr}
        tmp_base_res_name="result_thr_${tmp_thr}_${tmp_base_name}"
        tmp_base_res_path="${RES_DIR}/${tmp_base_res_name}"
        echo "Running experiments for ${tmp_base_res_name} with ${size_mb_used} MB used"

        # baseline
        EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline.exe"
        ${EXE_NAME} ${n_ker} 0 0 0 &> "${tmp_base_res_path}_baseline.log"

        # ALL=HBW
        EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline-ALL-HBW.exe"
        ${EXE_NAME} ${n_ker} 0 0 0 &> "${tmp_base_res_path}_baseline-ALL-HBW.log"

        for tmp_step in $(seq 1 1 $((${N_STEPS}-1)))
        do
            cur_limit_mb="$((${size_mb_used}/${N_STEPS}*${tmp_step}+2))"
            echo "Running experiments for ${tmp_base_res_name} with ${size_mb_used} MB used - Limit = ${cur_limit_mb} MB"

            # FIFO
            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="${cur_limit_mb}" \
            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="${cur_limit_mb}" \
                ${EXE_NAME} ${n_ker} 0 0 0 &> "${tmp_base_res_path}_lim_${cur_limit_mb}_FIFO.log"

            # SIZE
            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
            H2M_TOOL_LIBRARIES="${TOOLS_DIR}/commit-strategy_size/tool.so" \
            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="${cur_limit_mb}" \
            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="${cur_limit_mb}" \
                ${EXE_NAME} ${n_ker} 0 0 0 &> "${tmp_base_res_path}_lim_${cur_limit_mb}_SIZE.log"

            # PRIORITY
            EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe"
            H2M_TOOL_LIBRARIES="${TOOLS_DIR}/commit-strategy_priority/tool.so" \
            H2M_MAX_MEM_CAP_OVERALL_MB_HBW="${cur_limit_mb}" \
            H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT="${cur_limit_mb}" \
                ${EXE_NAME} ${n_ker} 0 0 0 &> "${tmp_base_res_path}_lim_${cur_limit_mb}_PRIORITY.log"
        done
    done
done
