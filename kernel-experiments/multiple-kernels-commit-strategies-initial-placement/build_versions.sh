#!/usr/local_rwth/bin/zsh

# ========================================
# Variables
# ========================================
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )" # get path of current script
SRC_DIR=/work/jk869269/repos/hpc-projects/h2m/temporary-micro-benchmarks/applications/run-multiple-kernels
BIN_DIR=${BIN_DIR:-"${CUR_DIR}/binaries"}

echo "#============================================="
echo "#=== Directories"
echo "#============================================="
echo "Script Dir: ${CUR_DIR}"
echo "Source Dir: ${SRC_DIR}"
echo "Bin Dir: ${BIN_DIR}"

# create result directory if not existing
mkdir -p ${BIN_DIR}

echo "#============================================="
echo "#=== Build Application"
echo "#============================================="
tmp_base_exe_name="run-multiple-kernels"

# baseline
EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline.exe" \
COMPILE_FLAGS="-DDO_BASELINE=1" make -C ${SRC_DIR}

# ALL=HBW
EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}_baseline-ALL-HBW.exe" \
COMPILE_FLAGS="-DDO_BASELINE=1 -DMEM_SPACE_A=HBW -DMEM_SPACE_B=HBW -DMEM_SPACE_C=HBW" make -C ${SRC_DIR}

# FIFO / SIZE / PRIORITY
EXE_NAME="${BIN_DIR}/${tmp_base_exe_name}.exe" \
make -C ${SRC_DIR}
