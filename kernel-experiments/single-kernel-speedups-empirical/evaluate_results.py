import argparse
import os, sys
import numpy as np
import statistics as st
import matplotlib.pyplot as plt
import csv

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','..','common')))
from PlotFunctions import *

class CFileMetaData():

    def __init__(self, file_path, file_name):
        # get file name and split
        file_name = file_name.split(".")[0]
        tmp_split = file_name.split("_")
        
        self.n_threads      = int(tmp_split[2])
        self.version        = tmp_split[-1]
        self.repetitions    = int(tmp_split[-2])
        self.size           = int(tmp_split[-4])
        self.stride         = None
        if "_stride_" in file_name:
            self.stride     = int(tmp_split[-6])
        self.kernel     = "-".join(tmp_split[3:-5])

        self.id_dc          = None
        self.time_avg       = 0.0
        self.time_min       = 0.0
        self.time_max       = 0.0

        with open(file_path) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if "Data Container:" in line:
                tmp_split = line.split()
                self.id_dc = tmp_split[2].strip()
                continue
            if "avg:" in line:
                tmp_split = line.split()
                self.time_avg = float(tmp_split[1].strip())
                continue
            if "min:" in line:
                tmp_split = line.split()
                self.time_min = float(tmp_split[1].strip())
                continue
            if "max:" in line:
                tmp_split = line.split()
                self.time_max = float(tmp_split[1].strip())
                continue

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parses and summarizes execution time measurement results")
    parser.add_argument("-s", "--source",       required=True,  type=str, metavar="<folder path>", help=f"source folder containing outputs")
    parser.add_argument("-d", "--destination",  required=False, type=str, metavar="<folder path>", default=None, help=f"destination folder where resulting data and plots will be stored")
    args = parser.parse_args()

    if not os.path.exists(args.source):
        print(f"Source folder path \"{args.source}\" does not exist")
        sys.exit(1)

    # ========== DEBUG ==========
    # args.source = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m\\00_data\\h2m-experiments-data\\kernels-speedups-empirical\\2022-08-08_Optane"
    # ========== DEBUG ==========

     # save results in source folder
    if args.destination is None:
        args.destination = os.path.join(args.source, "result_evaluation")

    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
    
    target_folder_plot = os.path.join(args.destination, "plots")
    if not os.path.exists(target_folder_plot):
        os.makedirs(target_folder_plot)

    list_files = []
    for file in os.listdir(args.source):
        if file.endswith(".log") and "result_" in file:
            cur_file_path = os.path.join(args.source, file)
            print(cur_file_path)

            # read file meta data
            file_meta = CFileMetaData(cur_file_path, file)
            list_files.append(file_meta)

    # get unique combinations
    unique_kernels      = sorted(list(set([x.kernel for x in list_files])))

    for ker in unique_kernels:
        files_kernel        = [x for x in list_files if x.kernel == ker]
        unique_n_threads    = sorted(list(set([x.n_threads for x in files_kernel])))
        unique_versions     = sorted(list(set([x.version for x in files_kernel])))
        unique_sizes        = sorted(list(set([x.size for x in files_kernel])))
        unique_sizes_label  = [f"{(ms*ms*3*8/1000.0/1000.0)} MB" for ms in unique_sizes]
        unique_sizes_label_c= [f"{ms}\n{(ms*ms*3*8/1000.0/1000.0)} MB" for ms in unique_sizes]

        # ========================================
        # === View per matrix size
        # ========================================
        target_file_avg     = os.path.join(args.destination, f"data_{ker}_per_matrix_size_time-avg.csv")

        with open(target_file_avg, mode="w", newline='') as f_avg:
            writer_avg = csv.writer(f_avg, delimiter=';')

            for idx_ms in range(len(unique_sizes)):
                ms = unique_sizes[idx_ms]
                writer_avg.writerow(  [ f"========== Matrix Size {ms} ({unique_sizes_label[idx_ms]}) =========="])
                sub = [x for x in files_kernel if x.size == ms]
                
                # write header
                header = ['Versions/Threads']
                for i in range(len(unique_n_threads)):
                    header.append(str(unique_n_threads[i]))
                writer_avg.writerow(header)

                map_vals_avg        = {}
                arr_versions        = []
                arr_data_avg        = []
                arr_data_avg_s      = []

                for cur_ver in unique_versions:
                    sub2                = [x for x in sub if x.version == cur_ver]
                    tmp_data_avg        = [np.nan for x in range(len(unique_n_threads))]

                    for i in range(len(unique_n_threads)):
                        cur_thr         = unique_n_threads[i]
                        sub3            = [x for x in sub2 if x.n_threads == cur_thr]
                        tmp_data_avg[i] = st.mean([x.time_avg for x in sub3])

                    # write to csv
                    writer_avg.writerow([cur_ver] + tmp_data_avg)
                    # remember assignment
                    map_vals_avg[cur_ver]   = tmp_data_avg
                    arr_versions.append(cur_ver)
                    arr_data_avg.append(tmp_data_avg)
                
                # separator rows
                writer_avg.writerow([])

                # print speedups
                for cur_ver in unique_versions:
                    tmp_s_avg   = [map_vals_avg["baseline"][x] / map_vals_avg[cur_ver][x] for x in range(len(map_vals_avg[cur_ver]))]
                    # write to csv
                    writer_avg.writerow([f"Speedup_{cur_ver}"] + tmp_s_avg)
                    arr_data_avg_s.append(tmp_s_avg)
                
                # separator rows
                writer_avg.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_{ker}_time-avg_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_data_avg, f"Avg execution time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Execution time [sec]")

                tmp_target_file_name = f"plot_{ker}_speedup-avg_matsize_{ms}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_n_threads, arr_versions, arr_data_avg_s, f"Speedups avg execution time for matrix size {ms} ({unique_sizes_label[idx_ms]})", "# Threads", "Speedup")
    
        
        # ========================================
        # === View per number of threads
        # ========================================
        target_file_avg     = os.path.join(args.destination, f"data_{ker}_per_threads_time-avg.csv")

        with open(target_file_avg, mode="w", newline='') as f_avg:
            writer_avg = csv.writer(f_avg, delimiter=';')

            for cur_thr in unique_n_threads:
                writer_avg.writerow([ f"========== # Threads: {cur_thr} =========="])
                sub = [x for x in files_kernel if x.n_threads == cur_thr]
                
                # write header
                header = ['Versions/Size']
                for i in range(len(unique_sizes)):
                    ms = unique_sizes[i]
                    header.append(f"{unique_sizes[i]} ({unique_sizes_label[i]})")
                writer_avg.writerow(header)

                map_vals_avg        = {}
                arr_versions        = []
                arr_data_avg        = []
                arr_data_avg_s      = []

                for cur_ver in unique_versions:
                    sub2                = [x for x in sub if x.version == cur_ver]
                    tmp_data_avg        = [np.nan for x in range(len(unique_sizes))]

                    for i in range(len(unique_sizes)):
                        ms              = unique_sizes[i]
                        sub3            = [x for x in sub2 if x.size == ms]
                        tmp_data_avg[i] = st.mean([x.time_avg for x in sub3])

                    # write to csv
                    writer_avg.writerow([cur_ver] + tmp_data_avg)
                    # remember assignment
                    map_vals_avg[cur_ver]   = tmp_data_avg
                    arr_versions.append(cur_ver)
                    arr_data_avg.append(tmp_data_avg)

                # separator rows
                writer_avg.writerow([])

                # print speedups
                for cur_ver in unique_versions:
                    tmp_s_avg   = [map_vals_avg["baseline"][x] / map_vals_avg[cur_ver][x] for x in range(len(map_vals_avg[cur_ver]))]
                    # write to csv
                    writer_avg.writerow([f"Speedup_{cur_ver}"] + tmp_s_avg)
                    arr_data_avg_s.append(tmp_s_avg)
                
                # separator rows
                writer_avg.writerow([])

                # plot signals
                tmp_target_file_name = f"plot_{ker}_time-avg_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_versions, arr_data_avg, f"Avg execution time for {cur_thr} threads", "Matrix Size", "Execution time [sec]")

                tmp_target_file_name = f"plot_{ker}_speedup-avg_nthreads_{cur_thr}.png"
                tmp_target_file_path = os.path.join(target_folder_plot, tmp_target_file_name)
                plot_data_normal(tmp_target_file_path, unique_sizes_label_c, arr_versions, arr_data_avg_s, f"Speedups avg execution time for {cur_thr} threads", "Matrix Size", "Speedup")